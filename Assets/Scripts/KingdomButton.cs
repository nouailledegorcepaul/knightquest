﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class KingdomButton : MonoBehaviour{

    public Player player;
    private Animator animator;
    private GameObject requirementText;
    private GameObject levelNumberText;
    private bool spamProof = false;

    private GameObject profileButton;
    private GameObject forgeButton;
    private GameObject shopButton;
    private GameObject farmButton;
    private GameObject playButton;

    void Awake(){
      this.levelNumberText = GameObject.Find("Level Number");
      this.requirementText = GameObject.Find("Requirements");
      this.requirementText.GetComponent<TextMeshProUGUI>().text = "";
    }
    void Start(){
      this.profileButton = GameObject.Find("Profile Button");
      this.farmButton = GameObject.Find("Farm Button");
      this.shopButton = GameObject.Find("Shop Button");
      this.forgeButton = GameObject.Find("Forge Button");
      this.playButton = GameObject.Find("Play Button");
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
      this.levelNumberText.GetComponent<TextMeshProUGUI>().text = "Level "+(player.GetCurrentLevel());
      this.requirementText.SetActive(false);
      this.animator = GameObject.Find("Transition").GetComponent<Animator>();
      if ((this.player.GetCurrentLevel()==1 && !this.player.SaveTutorialDone()) || !this.player.SaveTutorialDone()){
        StartCoroutine(TutorialPriority());
      }
      if (this.player.AutoSaveEnabled()){
        this.player.SavePlayer("auto-"+this.player.GetName());
      }
    }

    void Update(){
      if (!player.HasFarmLevelsUnlocked()){
        this.farmButton.SetActive(false);
      }
      else{
        this.farmButton.SetActive(true);
      }
      if (!player.HasRescuedBlacksmith()){
        this.forgeButton.GetComponentInChildren<TextMeshProUGUI>().text = "Craft";
      }
      else{
        this.forgeButton.GetComponentInChildren<TextMeshProUGUI>().text = "Forge";
      }
      if (!player.HasRescuedShopKeeper()){
        this.shopButton.SetActive(false);
      }
      else{
        this.shopButton.SetActive(true);
      }
    }

    public IEnumerator TutorialPriority(){
      TutorialWindow tw = GameObject.Find("Tutorial Explanations").GetComponent<TutorialWindow>();
      while(tw.IsActive()){
        this.profileButton.GetComponent<Button>().interactable=false;
        this.farmButton.GetComponent<Button>().interactable=false;
        this.shopButton.GetComponent<Button>().interactable=false;
        this.forgeButton.GetComponent<Button>().interactable=false;
        this.playButton.GetComponent<Button>().interactable=false;
        yield return null;
      }
      AutoSaveWindow autoSaveWindow = GameObject.Find("Auto Save Confirm").GetComponent<AutoSaveWindow>();
      autoSaveWindow.Open();
      while(autoSaveWindow.IsOpened()){
        yield return null;
      }
      this.profileButton.GetComponent<Button>().interactable=true;
      this.forgeButton.GetComponent<Button>().interactable=true;
      this.farmButton.GetComponent<Button>().interactable=true;
      this.shopButton.GetComponent<Button>().interactable=true;
      this.playButton.GetComponent<Button>().interactable=true;
      this.player.SaveTutorial();
    }

    public IEnumerator SpamCD(){
      this.spamProof = true;
      yield return new WaitForSeconds(1f);
      this.spamProof = false;
    }


    public IEnumerator DisplayRequirements(Level level){
      if (!this.spamProof){
        StartCoroutine(SpamCD());
        string textContent = "Requirements :\n\n";
        foreach (string resource in level.GetRequierement().GetResources()){
          textContent += resource+"\n";
        }
        this.requirementText.SetActive(true);
        this.requirementText.GetComponent<TextMeshProUGUI>().text = textContent;
        yield return new WaitForSeconds(2f);
        this.requirementText.GetComponent<TextMeshProUGUI>().text = "";
        this.requirementText.SetActive(false);
      }
    }

    public void GoToMainLevel(){
      Level level = this.player.GetWorld().GetLevels()[this.player.GetCurrentLevel()];
      if (level.GetRequierement()!=null){
        // LEVEL REQUIREMENT ACCESS
        if (!level.GetRequierement().IsCompleted(this.player)){
          level.GetRequierement().GoalAchieved(this.player);
          if (!this.spamProof){
            if (level.GetRequierement().First(this.player)){
              this.player.GetDialogueBox().StartDialogue(level.GetRequierement().GetStartDialogue());
              StartCoroutine(DialoguePriority(level));
            }
            else{
              StartCoroutine(DisplayRequirements(level));
            }
          }
        }
        else{
          if (!level.GetRequierement().End(this.player)){
            level.GetRequierement().EndDone(this.player);
            this.player.GetDialogueBox().StartDialogue(level.GetRequierement().GetEndDialogue());
            StartCoroutine(DialoguePriority(level));
          }
          else{
            this.player.SetLevel(level);
            StartCoroutine(SceneTransition("BoardScene"));
          }
        }
      }
      // NORMAL ACCESS
      else{
        this.player.SetLevel(level);
        StartCoroutine(SceneTransition("BoardScene"));
      }
    }

    public void GoToProfile(){
      StartCoroutine(SceneTransition("ProfileScene"));
    }

    public void GoToShop(){
      StartCoroutine(SceneTransition("ShopScene"));
    }

    public void GoToForge(){
      StartCoroutine(SceneTransition("ForgeScene"));
    }

    public void GoToFarm(){
      StartCoroutine(SceneTransition("FarmScene"));
    }

    public IEnumerator SceneTransition(string scene){
      if (!this.spamProof){
        StartCoroutine(SpamCD());
        this.animator.SetTrigger("Play");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(scene);
      }
    }

    public IEnumerator DialoguePriority(Level level){
      while(!this.player.GetDialogueBox().DialogueEnded()){
        yield return null;
      }
      if (level.GetRequierement().First(this.player)){
        level.GetRequierement().FirstDone(this.player);
        StartCoroutine(DisplayRequirements(level));
      }
      else{
        if (level.GetRequierement().IsCompleted(this.player)){
          this.player.SetLevel(level);
          StartCoroutine(SceneTransition("BoardScene"));
        }
        else{
          StartCoroutine(DisplayRequirements(level));
        }
      }

    }

}
