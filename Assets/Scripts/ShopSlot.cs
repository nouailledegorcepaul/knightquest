using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ShopSlot : MonoBehaviour{

    public ShopList parent;
    public GameObject image;
    public GameObject button;
    public Sprite sprite;
    private string resourceName= "";
    public Recipe recipe;
    public Player player;
    public GameObject recipeContainer;
    public Level[] requiredLevels;
    public bool amountChanger = false;
    private List<int> amounts;
    private int index;
    private int amount;
    private Dictionary<string, int> currentRecipe;

    public void Start(){
      this.amounts = new List<int>();
      this.amounts.Add(1);
      this.amounts.Add(10);
      this.amounts.Add(100);
      this.amounts.Add(-1);
      this.index = 0;
      this.amount = 1;
      this.parent = GameObject.Find("Shop List").GetComponent<ShopList>();
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
      this.image.GetComponent<Image>().sprite = sprite;
      this.resourceName = sprite.name;
      if (this.requiredLevels.Length>0){
        this.gameObject.SetActive(false);
        if (this.GetRequiredLevels().Contains(this.player.GetLevel())){
          Requirement req = this.player.GetLevel().GetRequierement();
          this.gameObject.SetActive(!req.GoalAchieved(this.player));
        }
      }
      this.currentRecipe = this.GetBaseRecipe();
    }

    public Dictionary<string, int> GetBaseRecipe(){
      return this.recipe.GetRecipe();
    }

    public Dictionary<string, int> GetCurrentRecipe(){
      return this.currentRecipe;
    }

    public Level[] GetRequiredLevels(){
      return this.requiredLevels;
    }

    public void Buy(){
      if (this.player.RecipeComplete(this.currentRecipe)){
        this.player.Craft(this.currentRecipe);
        this.player.AddResources(resourceName, this.amount);
        if (this.index == this.amounts.Count-1){
          ChangeAmount();
        }
        else{
          DisplayRecipe();
        }
      }
      if (this.GetRequiredLevels().Length>0){
        this.parent.ClearCraftContainer();
        this.gameObject.SetActive(false);
        this.parent.SetForgeButtonActive(false);
        this.parent.SetAmountButtonActive(false);
      }
    }

    public void ChangeAmount(){
      this.index++;
      if (this.index==this.amounts.Count){
        this.index=0;
      }
      this.amount = this.amounts[this.index];
      this.parent.SetAmountButtonText("x"+this.amount);
      if (this.amount==-1){
        this.parent.SetAmountButtonText("Max");
        RefreshMaxAmount();
      }
      else{
        foreach (string element in this.GetBaseRecipe().Keys){
          this.currentRecipe[element] = this.GetBaseRecipe()[element] * this.amount;
        }
      }
      DisplayRecipe();
    }

    public void RefreshMaxAmount(){
      bool found = false;
      foreach(string element in this.GetBaseRecipe().Keys){
        if (this.GetBaseRecipe()[element]>0){
          int maxAmount = this.player.GetResources()[element] / this.GetBaseRecipe()[element];
          if (maxAmount==0){
            break;
          }
          foreach (string element2 in this.GetBaseRecipe().Keys){
            if (this.GetBaseRecipe()[element2]>0){
              this.currentRecipe[element2] = this.GetBaseRecipe()[element2] * maxAmount;
            }
          }
          if (this.player.RecipeComplete(this.currentRecipe)){
            this.amount = maxAmount;
            found = true;
            break;
          }
        }
      }
      if (!found){
        this.currentRecipe = this.GetBaseRecipe();
      }
    }

    public void DisplayRecipe(){
      this.parent.ClearCraftContainer();
      if (this.currentRecipe!=null){
        GameObject container = GameObject.Find("Resources Needed");
        List<GameObject> recipeList = new List<GameObject>();
        foreach(string element in this.currentRecipe.Keys){
          if (element!="gold" && this.currentRecipe[element]>0){
            GameObject elem = Instantiate(this.recipeContainer, container.transform.position, Quaternion.identity);
            elem.name = element + "container";
            GameObject elemImage = new GameObject(element + " image");
            elemImage.AddComponent<Image>();
            elemImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("Art/Icons/"+element);
            elemImage.GetComponent<Image>().preserveAspect = true;
            GameObject amount = new GameObject(element + " amount");
            amount.AddComponent<TextMeshProUGUI>();
            amount.GetComponent<TextMeshProUGUI>().SetText(this.player.GetResources()[element] + "/" + this.currentRecipe[element].ToString());
            amount.GetComponent<TextMeshProUGUI>().fontSize = 20f;
            elemImage.transform.SetParent(elem.transform);
            amount.transform.SetParent(elem.transform);
            elem.transform.SetParent(container.transform);
            recipeList.Add(elem);
          }
        }
        for (int i = 0; i<recipeList.Count; i++){
          if (recipeList[i]!=null){
            recipeList[i].transform.localScale = new Vector3(2f, 2f, 2f)/recipeList.Count*1.3f;
            if (recipeList.Count==1){
              recipeList[i].transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            }
          }
        }
        if (this.player.RecipeComplete(this.currentRecipe)){
          this.parent.SetForgeButtonColor("green");
          this.parent.SetForgeButtonInteractable(true);
        }
        else{
          this.parent.SetForgeButtonColor("red");
          this.parent.SetForgeButtonInteractable(false);
        }
        this.parent.SetForgeButtonText(this.currentRecipe["gold"] + "g");
        this.parent.GetForgeButton().GetComponent<Button>().onClick.RemoveAllListeners();
        this.parent.GetForgeButton().GetComponent<Button>().onClick.AddListener(Buy);
        this.parent.GetAmountButton().GetComponent<Button>().onClick.RemoveAllListeners();
        this.parent.GetAmountButton().GetComponent<Button>().onClick.AddListener(ChangeAmount);
      }
      this.parent.SetForgeButtonActive(this.currentRecipe!=null);
      this.parent.SetAmountButtonActive(this.amountChanger);
    }
}
