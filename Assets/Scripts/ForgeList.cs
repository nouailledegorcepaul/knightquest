﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class ForgeList : MonoBehaviour{

  public GameObject forgeButton;
  public GameObject[] artisanSlots;
  public GameObject[] doctorSlots;
  
  private TextMeshProUGUI playerGold;
  private Player player;
  private GameObject magicSlot;
  private GameObject blacksmithText;
  private GameObject artisanText;
  private GameObject doctorText;


  void Start(){
    this.player = GameObject.FindWithTag("player").GetComponent<Player>();
    this.playerGold = GameObject.Find("Gold Container").GetComponentInChildren<TextMeshProUGUI>();
    this.magicSlot = GameObject.Find("Magic Item");
    this.blacksmithText = GameObject.Find("Blacksmith");
    this.artisanText = GameObject.Find("Artisan");
    this.doctorText = GameObject.Find("Doctor");

    this.magicSlot.SetActive(this.player.HasRescuedBlacksmith());
    this.blacksmithText.GetComponent<TextMeshProUGUI>().text = "Craft";
    if (this.player.HasRescuedBlacksmith()){
      this.blacksmithText.GetComponent<TextMeshProUGUI>().text = "Blacksmith";
    }
    foreach (GameObject go in artisanSlots){
      go.SetActive(this.player.HasRescuedArtisan());
    }
    this.artisanText.SetActive(this.player.HasRescuedArtisan());
    foreach (GameObject go in doctorSlots){
      go.SetActive(this.player.HasRescuedDoctor());
    }
    this.doctorText.SetActive(this.player.HasRescuedDoctor());
    this.forgeButton.SetActive(false);
  }

  void Update(){
    if (Input.GetKeyDown(KeyCode.Escape)){
      SceneManager.LoadScene("KingdomScene");
    }
    this.playerGold.text = "" + this.player.GetGold();
  }

  //[----------FORGE BUTTON----------]
  public GameObject GetForgeButton(){
    return this.forgeButton;
  }

  public void SetButtonActive(bool active){
    this.forgeButton.SetActive(active);
  }

  public void SetButtonInteractable(bool interactable){
    this.forgeButton.GetComponent<Button>().interactable = interactable;
  }

  public void SetButtonText(string text){
    this.forgeButton.GetComponentInChildren<TextMeshProUGUI>().SetText(text);
  }

  public string GetButtonText(){
    return this.forgeButton.GetComponentInChildren<TextMeshProUGUI>().text;
  }

  public void SetButtonColor(string color){
    if (color=="red"){
      this.forgeButton.GetComponent<Image>().color = new Color32(171, 32, 22, 255);
    }
    else{
      this.forgeButton.GetComponent<Image>().color = new Color32(17, 121, 15, 255);
    }
  }
  //[----------FORGE BUTTON----------]
  public void ClearCraftContainer(){
    GameObject container = GameObject.Find("Resources Needed");
    foreach (Transform child in container.transform){
      Destroy(child.gameObject);
    }
  }
}
