using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class ShopList : MonoBehaviour{

  public GameObject forgeButton;
  public GameObject amountButton;
  private TextMeshProUGUI playerGold;
  private Player player;

  void Start(){
    this.player = GameObject.FindWithTag("player").GetComponent<Player>();
    this.playerGold = GameObject.Find("Gold Container").GetComponentInChildren<TextMeshProUGUI>();
    this.forgeButton.SetActive(false);
    this.amountButton.SetActive(false);
  }

  void Update(){
    if (Input.GetKeyDown(KeyCode.Escape)){
      SceneManager.LoadScene("KingdomScene");
    }
    this.playerGold.text = "" + this.player.GetGold();
  }

  //[----------FORGE BUTTON----------]
  public GameObject GetForgeButton(){
    return this.forgeButton;
  }

  public GameObject GetAmountButton(){
    return this.amountButton;
  }

  public void SetForgeButtonActive(bool active){
    this.forgeButton.SetActive(active);
  }

  public void SetAmountButtonActive(bool active){
    this.amountButton.SetActive(active);
  }

  public void SetForgeButtonInteractable(bool interactable){
    this.forgeButton.GetComponent<Button>().interactable = interactable;
  }

  public void SetForgeButtonText(string text){
    this.forgeButton.GetComponentInChildren<TextMeshProUGUI>().SetText(text);
  }

  public void SetAmountButtonText(string text){
    this.amountButton.GetComponentInChildren<TextMeshProUGUI>().SetText(text);
  }

  public string GetForgeButtonText(){
    return this.forgeButton.GetComponentInChildren<TextMeshProUGUI>().text;
  }

  public void SetForgeButtonColor(string color){
    if (color=="red"){
      this.forgeButton.GetComponent<Image>().color = new Color32(171, 32, 22, 255);
    }
    else{
      this.forgeButton.GetComponent<Image>().color = new Color32(17, 121, 15, 255);
    }
  }
  //[----------FORGE BUTTON----------]
  public void ClearCraftContainer(){
    GameObject container = GameObject.Find("Resources Needed");
    foreach (Transform child in container.transform){
      Destroy(child.gameObject);
    }
  }
}
