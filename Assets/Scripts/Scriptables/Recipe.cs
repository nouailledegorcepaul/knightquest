using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Recipe", menuName = "Recipe")]
public class Recipe : ScriptableObject{

  public int wood;
  public int leather;
  public int stone;
  public int magicStone;
  public int gold;

  public Dictionary<string, int> GetRecipe(){
    Dictionary<string, int> recipe = new Dictionary<string, int>();
    recipe.Add("wood", wood);
    recipe.Add("leather", leather);
    recipe.Add("stone", stone);
    recipe.Add("magic_stone", magicStone);
    recipe.Add("gold", gold);

    return recipe;
  }
}
