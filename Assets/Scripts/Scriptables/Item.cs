using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Item", menuName = "Item")]
public class Item : ScriptableObject{

  public int id = -1;
  public GameObject prefab;

  public string itemType;
  public string itemName;
  public int damage;
  public int magic;
  public int shield;
  public int heal;
  public int defense;
  public int magicDefense;
  public int hp;
  public Recipe recipe;

  public string GetItemName(){return this.itemName;}
  public string GetItemType(){return this.itemType;}
  public int GetId(){return this.id;}
  public int GetDamage(){return this.damage;}
  public int GetMagic(){return this.magic;}
  public int GetShield(){return this.shield;}
  public int GetHeal(){return this.heal;}
  public int GetHp(){return this.hp;}
  public int GetDefense(){return this.defense;}
  public int GetMagicDefense(){return this.magicDefense;}
  public GameObject GetPrefab(){return this.prefab;}

  public Dictionary<string, int> GetRecipe(){
    if (this.recipe!=null){
      return this.recipe.GetRecipe();
    }
    return null;
  }

  public void SetId(int id){this.id = id;}
}
