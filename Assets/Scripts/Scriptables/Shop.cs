﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shop", menuName = "Shop")]
public class Shop : ScriptableObject{

  public Item[] damageItems;
  public Item[] magicItems;
  public Item[] shieldItems;
  public Item[] healItems;

  public Item[] GetDamageItems(){
    return this.damageItems;
  }

  public Item[] GetMagicItems(){
    return this.magicItems;
  }

  public Item[] GetShieldItems(){
    return this.shieldItems;
  }

  public Item[] GetHealItems(){
    return this.healItems;
  }
}
