using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Resource", menuName = "Resource")]
public class Resource : ScriptableObject{

  public GameObject prefab;

  public string itemType;
  public string itemName;


  public string GetItemName(){
    return this.itemName;
  }
  public string GetItemType(){
    return this.itemType;
  }

  public GameObject GetPrefab(){
    return this.prefab;
  }
}
