using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Level")]
public class Level : ScriptableObject{

  public GameObject enemyPrefab;

  public Dialogue startDialogue = null;
  public Dialogue endDialogue = null;
  public string category = "main";
  public string levelName = "None";
  public int gold;
  public int exp;
  public int leather;
  public int wood;
  public int stone;
  public int moves = -1;
  public int levelRequirement = -1;
  public Requirement requirement;
  public bool blacksmith = false;
  public bool shopKeeper = false;
  public bool artisan = false;
  public bool doctor = false;

  public string[] names;
  [TextArea(3, 10)]
  public string[] sentences;

  private Dictionary<string, string[]> difficultyText = new Dictionary<string, string[]>();
  private Dictionary<string, int> rewards = new Dictionary<string, int>();
  private int baseExp;

  public void OnEnable(){
    this.baseExp = this.exp;
    this.rewards.Add("exp", this.exp);
    this.rewards.Add("gold", this.gold);
    this.rewards.Add("leather", this.leather);
    this.rewards.Add("wood", this.wood);
    this.rewards.Add("stone", this.stone);
    if (this.category!="main" && this.sentences.Length>2 && this.names.Length>2){
      this.difficultyText.Add("easy", new string[2]{names[0], sentences[0]});
      this.difficultyText.Add("medium", new string[2]{names[1], sentences[1]});
      this.difficultyText.Add("hard", new string[2]{names[2], sentences[2]});
    }
  }

  public Dialogue GetStartDialogue(){
    return this.startDialogue;
  }

  public Dialogue GetEndDialogue(){
    return this.endDialogue;
  }

  public Dictionary<string, int> GetRewards(){
    return this.rewards;
  }

  public Enemy GetEnemy(){
    return this.enemyPrefab.GetComponent<Enemy>();
  }

  public string GetName(){
    return this.levelName;
  }

  public string GetCategory(){
    return this.category;
  }

  public int GetWood(){
    return this.wood;
  }

  public int GetLeather(){
    return this.leather;
  }

  public int GetStone(){
    return this.stone;
  }

  public int GetGold(){
    return this.gold;
  }

  public int GetExp(){
    return this.exp;
  }

  public int GetMoves(){
    return this.moves;
  }

  public int GetLevelRequirement(){
    return this.levelRequirement;
  }

  public Requirement GetRequierement(){
    return this.requirement;
  }

  public void RewardPlayer(Player player){
    player.AddResources(this.rewards);
    if (blacksmith){
      player.BlacksmithRescued();
    }
    else if (shopKeeper){
      player.ShopKeeperRescued();
    }
    else if (artisan){
      player.ArtisanRescued();
    }
    else if (doctor){
      player.DoctorRescued();
    }
  }

  public void PartialRewardPlayer(Player player){
    player.AddExperience(this.GetPartialExp());
  }

  public int GetPartialExp(){
    double percentage = Random.Range(5, 15)/100.0;
    return (int)(this.rewards["exp"]*percentage);
  }

  public void SetFarmLevelDifficulty(Player player, string difficulty){
    float multiplicateur = 0.0f;
    if (player.GetExpLevel()<10){
      multiplicateur = 1.2f;
    }
    else if (player.GetExpLevel()<20){
      multiplicateur = 1.25f;
    }
    else{
      multiplicateur = 1.3f;
    }
    multiplicateur = Mathf.Pow(multiplicateur, (float)player.GetExpLevel());
    float levelMultiplicateur = 1.0f;
    float finalMultiplicateur;

    if (difficulty=="hard"){
      levelMultiplicateur = 1.3f;
    }
    else if (difficulty=="medium"){
      levelMultiplicateur = 1.15f;
    }

    finalMultiplicateur = multiplicateur*levelMultiplicateur;
    this.AdjustFarmLevel(player, finalMultiplicateur);
    this.GetEnemy().SetName(GetDifficultyName(difficulty));
  }

  public void AdjustFarmLevel(Player player, double multiplicateur=0.0){
    this.rewards["exp"]=(int)(this.exp * multiplicateur);
    this.rewards["gold"]=(int)(this.gold * multiplicateur);
    this.rewards["leather"]=(int)(this.leather * multiplicateur);
    this.rewards["wood"]=(int)(this.wood * multiplicateur);
    this.rewards["stone"]=(int)(this.stone * multiplicateur);
  }

  public string GetDifficultyText(string difficulty){
    return this.difficultyText[difficulty][1];
  }

  public string GetDifficultyName(string difficulty){
    return this.difficultyText[difficulty][0];
  }
}
