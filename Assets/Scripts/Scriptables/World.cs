using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "World", menuName = "World")]
public class World : ScriptableObject{

  public Level[] levelArray;
  private List<Level> levels;
  private Level ranchLevel;
  private Level forestLevel;
  private Level caveLevel;
  private Level banditLevel;



  public void OnEnable(){
    this.levels = new List<Level>();
    foreach(Level level in levelArray){
      this.AddLevel(level);
    }
  }

  public void AddLevel(Level level){
    string category = level.GetCategory();
    if (category=="forest"){
      this.forestLevel = level;
    }
    else if (category=="ranch"){
      this.ranchLevel = level;
    }
    else if (category=="cave"){
      this.caveLevel = level;
    }
    else if (category=="bandit"){
      this.banditLevel = level;
    }
    else{
      this.levels.Add(level);
    }
  }

  public Level[] GetAllLevels(){
    return this.levelArray;
  }

  public List<Level> GetLevels(){
    return this.levels;
  }

  public List<Level> GetFarmLevels(){
    List<Level> farmLevels = new List<Level>();
    farmLevels.Add(this.forestLevel);
    farmLevels.Add(this.caveLevel);
    farmLevels.Add(this.banditLevel);
    farmLevels.Add(this.ranchLevel);
    return farmLevels;
  }

  public Level GetFarmLevel(string category){
    if (category=="forest"){
      return this.forestLevel;
    }
    if (category=="cave"){
      return this.caveLevel;
    }
    if (category=="ranch"){
      return this.ranchLevel;
    }
    else{
      return this.banditLevel;
    }
  }

}
