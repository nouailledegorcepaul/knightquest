using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Requirement", menuName = "Requirement")]
public class Requirement : ScriptableObject{

  public Dialogue startDialogue, endDialogue;
  public string[] resources;

  public bool GoalAchieved(Player player){
    foreach (string resource in this.resources){
      if (!player.HasResources(resource)){
        return false;
      }
    }
    player.SetLevelRequirement("complete", true);
    return true;
  }

  public string[] GetResources(){return this.resources;}
  public Dialogue GetStartDialogue(){return this.startDialogue;}
  public Dialogue GetEndDialogue(){return this.endDialogue;}
  
  public bool IsCompleted(Player player){return player.GetLevelRequirements()[player.GetCurrentLevel()]["complete"];}
  public bool First(Player player){return player.GetLevelRequirements()[player.GetCurrentLevel()]["first"];}
  public bool End(Player player){return player.GetLevelRequirements()[player.GetCurrentLevel()]["end"];}

  public void FirstDone(Player player){player.GetLevelRequirements()[player.GetCurrentLevel()]["first"]=false;}
  public void EndDone(Player player){player.GetLevelRequirements()[player.GetCurrentLevel()]["end"]=false;}

}
