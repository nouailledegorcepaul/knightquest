using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[CreateAssetMenu(fileName = "Dialogue", menuName = "Dialogue")]
public class Dialogue : ScriptableObject{

  public string speakerName = "Narrator";

  [TextArea(3, 10)]
  public string[] sentences;

  public string GetSpeakerName(){
    return this.speakerName;
  }

  public string[] GetSentences(){
    return this.sentences;
  }

  public void SetSpeakerName(string name){
    this.speakerName = name;
  }

}
