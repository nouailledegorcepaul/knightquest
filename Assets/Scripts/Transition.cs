using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Transition : MonoBehaviour{

    private Animator animator;

    void Start(){
      this.animator = GameObject.Find("Transition").GetComponent<Animator>();
    }

    public void GoToKingdom(){
      StartCoroutine(KingdomTransition());
    }

    public IEnumerator KingdomTransition(){
      this.animator.SetTrigger("Play");
      yield return new WaitForSeconds(1f);
      SceneManager.LoadScene("KingdomScene");
    }

    public void GoToProfile(){
      StartCoroutine(ProfileTransition());
    }

    public IEnumerator ProfileTransition(){
      this.animator.SetTrigger("Play");
      yield return new WaitForSeconds(1f);
      SceneManager.LoadScene("ProfileScene");
    }

    public void GoToShop(){
      SceneManager.LoadScene("ShopScene");
    }

    public void GoToForge(){
      SceneManager.LoadScene("ForgeScene");
    }

    public void GoToFarm(){
      SceneManager.LoadScene("FarmScene");
    }

}
