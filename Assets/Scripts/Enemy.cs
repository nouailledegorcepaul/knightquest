using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour{

  public int maxHealth;
  public int currentHealth;

  public int damage = 0;
  public int magic = 0;
  public int defense = 0;
  public int magicDefense = 0;

  public int baseHitRate = 50;
  private int hitRate;
  public string enemyName = "None";
  public string enemyCategory = "None";

  public HealthBar healthBar;


  //[------------------- INITALISATION FUNCTIONS --------------------]
  void Start(){
    this.hitRate = this.baseHitRate;
  }

  void Update(){}

  public void InitBars(GameObject healthBar){
    this.currentHealth = this.maxHealth;
    this.healthBar = healthBar.GetComponent<HealthBar>();
    if (this.maxHealth>0){
      this.healthBar.SetMaxHealth(this.maxHealth);
      this.healthBar.SetHealth(this.currentHealth);
    }
    else{
      this.healthBar.SetMaxHealth(1);
      this.healthBar.SetHealth(1);
    }
    this.healthBar.SetTextName(this.enemyName);
  }
  //[------------------- INITALISATION FUNCTIONS --------------------]

  //[----------------------- GETTER FUNCTIONS -----------------------]
  public string GetName(){return this.enemyName;}
  public int GetMaxHealth(){return this.maxHealth;}
  public int GetDamage(){return this.damage;}
  public int GetMagic(){return this.magic;}
  public int GetDefense(){return this.defense;}
  public int GetMagicDefense(){return this.magicDefense;}
  public double GetDamageReduction(){return 100.0/(100.0+GetDefense());}
  public double GetMagicDamageReduction(){return 100.0/(100.0+GetMagicDefense());}
  public bool IsAlive(){return this.currentHealth==-1 || this.currentHealth>0;}
  //[----------------------- GETTER FUNCTIONS -----------------------]


  //[----------------------- SETTER FUNCTIONS -----------------------]
  public void SetName(string name){
    this.enemyName = name;
  }

  public void SetMaxHealth(int maxHealth){
    this.maxHealth = maxHealth;
  }

  public void SetDamage(int damage){
    this.damage = damage;
  }

  public void SetMagic(int magic){
    this.magic = magic;
  }

  public void SubDamage(int damage){
    this.damage -= damage;
  }

  public void SubMagic(int magic){
    this.magic -= magic;
  }

  //[----------------------- SETTER FUNCTIONS -----------------------]

  //[----------------------- ACTION FUNCTIONS -----------------------]
  public bool Attack(Player player){
    if (this.baseHitRate>0){
      if (Random.Range(0, 100)<this.hitRate){
        player.TakeDamage(this);
        this.hitRate = this.baseHitRate;
        return true;
      }
      IncreaseHitRate();
    }
    return false;
  }



  public void TakeDamage(string type, int amount){
    if (type=="damage"){
      amount = (int)(amount * GetDamageReduction());
    }
    else{
      amount = (int)(amount * GetMagicDamageReduction());
    }
    if (this.currentHealth!=-1){
      this.currentHealth -= amount;
      this.healthBar.SetHealth(this.currentHealth);
    }
  }

  public void IncreaseHitRate(){
    if (hitRate<=50){
      this.hitRate+=15;
    }
    else if (hitRate<=70){
      this.hitRate+=10;
    }
    else{
      this.hitRate+=5;
    }
  }
  //[----------------------- ACTION FUNCTIONS -----------------------]
}
