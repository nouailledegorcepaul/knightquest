﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Element : MonoBehaviour  {

  public int column;
  public int row;

  private Board board;
  private Vector2 firstTouchPos;
  private Vector2 finalTouchPos;
  public bool isMatched;
  public bool isPowerUp;
  private int clicked = 0;
  private bool powerUpUsed = false;
  private bool unbreakable;
  private string elemTag;
  public float swipeAngle = 0;

  void Start(){
    this.board = FindObjectOfType<Board>();
    this.isMatched = false;
    this.isPowerUp = false;
    this.unbreakable = true;
    StartCoroutine(UnbreakableTime());
    this.elemTag = this.gameObject.tag;
  }

  // Update is called once per frame
  void Update(){
    Vector2 smoothTime = new Vector2(1.0f, 1.0f);
    float speed = 2.5f * Time.deltaTime;
    // if (tag!="damage" && tag!="magic" && tag!="heal" && tag!="shield" && this.unbreakable && this.board.GetState()==GameState.move){
    //   this.unbreakable = false;
    // }
    if (this.column<0 || this.column>=this.board.GetWidth() || this.row<0 || this.row>=this.board.GetHeight()){
      this.isPowerUp = true;
    }
    Vector2 tempPos;
    if (this.isPowerUp){
      speed = 12 * Time.deltaTime;
    }
    if (Mathf.Abs(this.column-transform.position.x) > .05){
      tempPos = new Vector2(this.column, transform.position.y);
      if (this.isPowerUp){
        transform.position = Vector2.MoveTowards(transform.position, tempPos, speed);
      }
      else{
        transform.position = Vector2.SmoothDamp(transform.position, tempPos, ref smoothTime, speed);
      }
    }
    else{
      tempPos = new Vector2(this.column, transform.position.y);
      transform.position = tempPos;
    }
    if (Mathf.Abs(this.row-transform.position.y) > .05){
      tempPos = new Vector2(transform.position.x, this.row);
      if (this.isPowerUp){
        transform.position = Vector2.MoveTowards(transform.position, tempPos, speed);
      }
      else{
        transform.position = Vector2.SmoothDamp(transform.position, tempPos, ref smoothTime, speed);
      }
    }
    else{
      tempPos = new Vector2(transform.position.x, this.row);
      transform.position = tempPos;
    }
    if (!isPowerUp){
      while (this.row>0 && this.board.GetElements()[this.column, this.row-1] == null && this.board.powerUpCurrentlyFired==0){
        this.board.GetElements()[this.column, this.row] = null;
        this.row --;
        this.board.GetElements()[this.column, this.row] = this.gameObject;
      }
    }
    else{
      float x = this.transform.position.x;
      float y = this.transform.position.y;
      if (x<-0.5 || x>this.board.width-0.5 || y<-0.5 ||y>this.board.height-0.5){
        Autodestruct();
      }
    }
  }

  public void PowerUpUsed(){
    this.powerUpUsed = true;
  }

  public bool IsUnbreakable(){
    return elemTag!="damage" && elemTag!="magic" && elemTag!="heal" && elemTag!="shield" && this.unbreakable;
  }

  public IEnumerator UnbreakableTime(){
    yield return new WaitForSeconds(0.1f);
    this.unbreakable = false;
  }



  public void OnTriggerEnter2D(Collider2D collider){
    string tag = collider.gameObject.tag;
    if (this.isPowerUp && !collider.gameObject.GetComponent<Element>().isPowerUp){
      int x = collider.gameObject.GetComponent<Element>().GetColumn();
      int y = collider.gameObject.GetComponent<Element>().GetRow();
      DestroyElementAt(x, y, tag=="rock");
    }
  }

  public void DestroyElementAt(int x, int y, bool autodestruct){
    this.board.DestroyElementAt(x, y, true);
    if (autodestruct){
      Autodestruct();
    }
  }

  public void Autodestruct(){
    this.board.powerUpCurrentlyFired--;
    if (this.board.powerUpCurrentlyFired==0){
      this.board.DestroyMatches();
    }
    Destroy(this.gameObject);
  }

  public bool IsPowerUpUsed(){
    return this.powerUpUsed;
  }

  public int GetRow(){
    return this.row;
  }

  public int GetColumn(){
    return this.column;
  }

  public bool IsMatched(){
    return this.isMatched;
  }

  public void SetRow(int row){
    this.row = row;
  }

  public void SetColumn(int column){
    this.column = column;
  }

  public void Match(){
    this.isMatched = true;
  }

  public void IncrementRow(){
    this.row += 1;
  }

  public void IncrementColumn(){
    this.column += 1;
  }

  public void DecrementRow(){
    this.row -= 1;
  }

  public void DecrementColumn(){
    this.column -= 1;
  }

  private void OnMouseDown(){
    firstTouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
  }

  private void OnMouseUp(){
    finalTouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    string tag = this.gameObject.tag;
    if (this.board.GetState()==GameState.move){
      if (firstTouchPos==finalTouchPos && (tag=="row_power-up" || tag=="column_power-up" || tag=="bomb_power-up" || tag=="super_power-up")){
        this.clicked++;
      }
      else{
        this.clicked=0;
      }

      if (this.clicked==2){
        this.clicked = 0;
        if (tag == "super_power-up"){
          this.board.ActivateSuperPowerUp(this.gameObject);
        }
        else if (tag == "bomb_power-up"){
          this.board.ActivateBombPowerUp(this.gameObject);
        }
        else{
          this.board.ActivatePowerUp(this.gameObject);
        }
      }
      else if (this.board.GetState() == GameState.move && finalTouchPos!=firstTouchPos && this.gameObject.tag!="rock"){
        CalculateAngle();
      }
    }
  }

  void CalculateAngle(){
    if (firstTouchPos!=finalTouchPos){
      swipeAngle = Mathf.Atan2(finalTouchPos.y - firstTouchPos.y, finalTouchPos.x - firstTouchPos.x) *180/ Mathf.PI;
      GameObject target = null;
      string direction = "";
      if (this.swipeAngle>-45 && this.swipeAngle <=45 && this.column<this.board.GetWidth()-1){//Right swap
        target = this.board.GetElements()[this.column+1, this.row];
        direction = "right";
      }
      else if(this.swipeAngle > 45 && this.swipeAngle <=135 && this.row<this.board.GetHeight()-1){
        target = this.board.GetElements()[this.column, this.row+1];
        direction = "up";
      }
      else if((this.swipeAngle > 135 || this.swipeAngle <=-135) && this.column>0){
        target = this.board.GetElements()[this.column-1, this.row];
        direction = "left";
      }
      else if((this.swipeAngle > -135 && this.swipeAngle <= -45) && this.row>0){
        target = this.board.GetElements()[this.column, this.row-1];
        direction = "down";
      }
      if (target != null){
        if (target.tag != "rock"){
          StartCoroutine(this.board.SwapElements(direction, this, target.GetComponent<Element>()));
        }
      }
    }
  }

}
