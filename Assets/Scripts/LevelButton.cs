using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour{

  public Board board;

  void Update(){
  }

  public void Restart(){
    this.board.Restart();
  }


  public void GameMainMenu(){
    SceneManager.LoadScene("MainMenuScene");
  }


  public void GoToKingdom(){
    StartCoroutine(ToKingdom());
  }

  public IEnumerator ToKingdom(){
    this.board.transition.SetTrigger("ToKingdom");
    yield return new WaitForSeconds(1f);
    SceneManager.LoadScene("KingdomScene");
  }
}
