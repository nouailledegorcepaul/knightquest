﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Profile : MonoBehaviour{

  public Player player;

  public GameObject playerStats;
  public GameObject playerGold;
  public GameObject playerStuff;
  public GameObject playerUpgradeButtons;
  public GameObject playerUpgradePointsText;
  public GameObject confirmUpgradeButton;

  public GameObject playerHeadSlot;
  public GameObject playerBodySlot;
  public GameObject playerPantsSlot;
  public GameObject playerBootsSlot;

  public GameObject playerDamageSlot;
  public GameObject playerMagicSlot;
  public GameObject playerShieldSlot;
  public GameObject playerHealSlot;

  public GameObject playerName;


  private Animator animator;
  private int currentPlayerPoints;
  private Dictionary<string, int> usedUpgradePoints;
  private bool enableUpgradeButtons;
  // private Color transparentColor;

  void Start(){
    this.player = GameObject.FindWithTag("player").GetComponent<Player>();
    this.currentPlayerPoints = this.player.GetUpgradePoints();
    this.usedUpgradePoints = new Dictionary<string, int>();
    this.usedUpgradePoints.Add("damage", 0);
    this.usedUpgradePoints.Add("magic", 0);
    this.usedUpgradePoints.Add("shield", 0);
    this.usedUpgradePoints.Add("heal", 0);
    this.enableUpgradeButtons = (this.currentPlayerPoints>0);
    this.animator = GameObject.Find("Transition").GetComponent<Animator>();
    if (this.playerName!=null){
      this.playerName.GetComponent<TextMeshProUGUI>().text = this.player.GetName();
    }
  }

  // Update is called once per frame
  void Update(){
    if (this.playerStats!=null){
      this.playerStats.transform.Find("Damage").GetComponent<TextMeshProUGUI>().SetText("Damage: "+NumberFact.Fact(this.player.GetDamage()));
      this.playerStats.transform.Find("Magic").GetComponent<TextMeshProUGUI>().SetText("Magic: "+NumberFact.Fact(this.player.GetMagic()));
      this.playerStats.transform.Find("Shield").GetComponent<TextMeshProUGUI>().SetText("Shield: "+NumberFact.Fact(this.player.GetShield()));
      this.playerStats.transform.Find("Heal").GetComponent<TextMeshProUGUI>().SetText("Heal: "+NumberFact.Fact(this.player.GetHeal()));
      GameObject.Find("Armor").GetComponent<TextMeshProUGUI>().SetText("Armor: "+NumberFact.Fact(this.player.GetDefense()) + "(-"+((1-this.player.GetDamageReduction())*100).ToString("#")+"%)");
      GameObject.Find("Magic Armor").GetComponent<TextMeshProUGUI>().SetText("Magic Armor: "+NumberFact.Fact(this.player.GetMagicDefense()) + "(-"+((1-this.player.GetMagicDamageReduction())*100).ToString("#")+"%)");
    }
    if (this.playerStuff!=null){
      if (this.player.GetHeadItem()==null){
        this.playerHeadSlot.SetActive(false);
      }
      else{
        this.playerHeadSlot.SetActive(true);
        this.playerHeadSlot.GetComponent<Image>().sprite = GetSprite(this.player.GetHeadItem());
      }
      if (this.player.GetBodyItem()==null){
        this.playerBodySlot.SetActive(false);
      }
      else{
        this.playerBodySlot.SetActive(true);
        this.playerBodySlot.GetComponent<Image>().sprite = GetSprite(this.player.GetBodyItem());
      }
      if (this.player.GetPantsItem()==null){
        this.playerPantsSlot.SetActive(false);
      }
      else{
        this.playerPantsSlot.SetActive(true);
        this.playerPantsSlot.GetComponent<Image>().sprite = GetSprite(this.player.GetPantsItem());
      }
      if (this.player.GetBootsItem()==null){
        this.playerBootsSlot.SetActive(false);
      }
      else{
        this.playerBootsSlot.SetActive(true);
        this.playerBootsSlot.GetComponent<Image>().sprite = GetSprite(this.player.GetBootsItem());
      }
      if (this.player.GetDamageItem()==null){
        this.playerDamageSlot.SetActive(false);
      }
      else{
        this.playerDamageSlot.SetActive(true);
        this.playerDamageSlot.GetComponent<Image>().sprite = GetSprite(this.player.GetDamageItem());
      }
      if (this.player.GetMagicItem()==null){
        this.playerMagicSlot.SetActive(false);
      }
      else{
        this.playerMagicSlot.SetActive(true);
        this.playerMagicSlot.GetComponent<Image>().sprite = GetSprite(this.player.GetMagicItem());
      }
      if (this.player.GetShieldItem()==null){
        this.playerShieldSlot.SetActive(false);
      }
      else{
        this.playerShieldSlot.SetActive(true);
        this.playerShieldSlot.GetComponent<Image>().sprite = GetSprite(this.player.GetShieldItem());
      }
      if (this.player.GetHealItem()==null){
        this.playerHealSlot.SetActive(false);
      }
      else{
        this.playerHealSlot.SetActive(true);
        this.playerHealSlot.GetComponent<Image>().sprite = GetSprite(this.player.GetHealItem());
      }
    }
    //[--------------UPGRADE BUTTONS--------------]
    if (this.playerUpgradeButtons!=null){
      //[--------------ADD BUTTONS--------------]
      this.playerUpgradeButtons.transform.Find("Add Buttons").transform.Find("Damage").GetComponent<Button>().interactable = this.currentPlayerPoints>0;
      this.playerUpgradeButtons.transform.Find("Add Buttons").transform.Find("Magic").GetComponent<Button>().interactable = this.currentPlayerPoints>0;
      this.playerUpgradeButtons.transform.Find("Add Buttons").transform.Find("Shield").GetComponent<Button>().interactable = this.currentPlayerPoints>0;
      this.playerUpgradeButtons.transform.Find("Add Buttons").transform.Find("Heal").GetComponent<Button>().interactable = this.currentPlayerPoints>0;

      //[--------------SUB BUTTONS--------------]
      this.playerUpgradeButtons.transform.Find("Sub Buttons").transform.Find("Damage").GetComponent<Button>().interactable = this.usedUpgradePoints["damage"]>0;
      this.playerUpgradeButtons.transform.Find("Sub Buttons").transform.Find("Magic").GetComponent<Button>().interactable = this.usedUpgradePoints["magic"]>0;
      this.playerUpgradeButtons.transform.Find("Sub Buttons").transform.Find("Shield").GetComponent<Button>().interactable = this.usedUpgradePoints["shield"]>0;
      this.playerUpgradeButtons.transform.Find("Sub Buttons").transform.Find("Heal").GetComponent<Button>().interactable = this.usedUpgradePoints["heal"]>0;
    }
    //[--------------UPGRADE POINTS TEXT--------------]
    if (this.playerUpgradePointsText!=null){
      this.playerUpgradePointsText.GetComponent<TextMeshProUGUI>().text = "Upgrade Points : "+this.player.GetUpgradePoints();
    }
    //[--------------PLAYER GOLD TEXT--------------]
    if (this.playerGold!=null){
      this.playerGold.GetComponentInChildren<TextMeshProUGUI>().SetText("Gold : "+this.player.GetGold());
    }
    //[-----CONFIRM BUTTON-----]
    if (this.confirmUpgradeButton!=null){
      this.confirmUpgradeButton.GetComponent<Button>().interactable = this.PlayerHasUsedUpgradePoints();
    }
  }

  public Sprite GetSprite(Item item){
    return item.GetPrefab().GetComponent<SpriteRenderer>().sprite;
  }

  public void AddBaseStats(string stat){
    this.usedUpgradePoints[stat]++;
    this.player.AddBaseStats(stat);
    this.currentPlayerPoints--;
  }

  public void SubBaseStats(string stat){
    this.usedUpgradePoints[stat]--;
    this.player.SubBaseStats(stat);
    this.currentPlayerPoints++;
  }

  public void ConfirmStats(){
    this.ResetUsedUpgradePoints();
    this.currentPlayerPoints=this.player.GetUpgradePoints();
    if (this.currentPlayerPoints==0){
      this.enableUpgradeButtons = false;
    }
  }

  public void ResetUsedUpgradePoints(){
    this.usedUpgradePoints["damage"]=0;
    this.usedUpgradePoints["magic"]=0;
    this.usedUpgradePoints["shield"]=0;
    this.usedUpgradePoints["heal"]=0;
  }

  public bool PlayerHasUsedUpgradePoints(){
    foreach (string stat in this.usedUpgradePoints.Keys){
      if (this.usedUpgradePoints[stat]>0){
        return true;
      }
    }
    return false;
  }

  public void GoToKingdom(){
    StartCoroutine(ToKingdom());
  }

  public IEnumerator ToKingdom(){
    this.animator.SetTrigger("Play");
    yield return new WaitForSeconds(1f);
    SceneManager.LoadScene("KingdomScene");
  }
}
