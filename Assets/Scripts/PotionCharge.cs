using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PotionCharge : MonoBehaviour{

  public Image fill;
  public Button button;
  public Player player;
  public int currentFillAmount = 0;
  public int maxFillAmount = 15;
  public int chargeHealAmount = 40;
  public int maxCharge = 3;
  public int currentCharge = 0;
  private Board board;

  void Start(){
    this.board = GameObject.Find("Board").GetComponent<Board>();
  }


  void Update(){
    this.fill.fillAmount = (float)this.currentFillAmount / (float)this.maxFillAmount;
    if (this.fill.fillAmount == 1f){
      if (this.currentCharge<this.maxCharge){
        this.currentCharge++;
        this.currentFillAmount = 0;
      }
    }
    if (this.currentCharge==0){
      this.button.interactable = false;
      this.button.GetComponent<Image>().color = new Color32(171, 32, 22, 255);
    }
    else{
      if (this.player.IsAlive()){
        this.button.interactable = this.board.playerTurn;
      }
      else{
        this.button.interactable = false;
      }
      this.button.GetComponent<Image>().color = new Color32(0, 169, 255, 255);
    }
    this.button.GetComponentInChildren<TextMeshProUGUI>().SetText(this.currentCharge + "/" + this.maxCharge);
    this.button.interactable = (this.board.GetState()==GameState.move && this.currentCharge>0);
  }

  public void UseCharge(){
    if (this.currentCharge>0){
      this.player.AddHealth(this.chargeHealAmount);
      this.currentCharge --;
    }
  }

  public void SetCurrentFillAmount(int amount){
    this.currentFillAmount = 0;
  }

  public void SetMaxFillAmount(int amount){
    this.maxFillAmount = amount;
  }

  public int GetMaxFillAmount(){
    return this.maxFillAmount;
  }

  public void SetChargeHealAmount(int amount){
    this.chargeHealAmount = amount;
  }

  public void SetMaxHealth(int health){
    this.maxFillAmount = health;
  }

  public void AddCurrentCharge(int health){
    this.currentFillAmount += health;
  }

  public void SetPlayer(Player player){
    this.player = player;
  }

  public void Reset(){
    this.currentFillAmount = 0;
    this.currentCharge = 0;
  }


}
