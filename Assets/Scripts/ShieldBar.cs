using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShieldBar : MonoBehaviour{

  public Slider slider;
  public Image fill;
  public GameObject textShield;


  public void SetMaxShield(int shield){
    slider.maxValue = shield;
    slider.value = shield;
  }

  public void SetShield(int shield){
    slider.value = shield;
    this.textShield.GetComponent<TextMeshProUGUI>().SetText(NumberFact.Fact(this.slider.value) + "/" + NumberFact.Fact(this.slider.maxValue));
  }

  public void Reset(){
    this.SetShield(0);
  }
}
