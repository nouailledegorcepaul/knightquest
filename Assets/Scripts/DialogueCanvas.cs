﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueCanvas : MonoBehaviour{

  private static DialogueCanvas instance = null;

  void Awake(){
    if (instance==null){
      DialogueCanvas.instance = this;
      DontDestroyOnLoad(this.gameObject);
    }
    else{
      Destroy(this.gameObject);
    }
  }

}
