﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBar : MonoBehaviour{

  public Slider slider;
  public Gradient gradient;
  public Image fill;
  public GameObject textHp;
  public GameObject textName;

  public GameObject shieldBar;


  void Update(){
    if (this.shieldBar!=null){
      if (this.shieldBar.GetComponent<Slider>().value>0){
        this.textHp.GetComponent<TextMeshProUGUI>().SetText(NumberFact.Fact(this.slider.value) + "(+" + NumberFact.Fact(this.shieldBar.GetComponent<Slider>().value) +")/" + NumberFact.Fact(this.slider.maxValue));
      }
      else{
        this.textHp.GetComponent<TextMeshProUGUI>().SetText(NumberFact.Fact(this.slider.value) + "/" + NumberFact.Fact(this.slider.maxValue));
      }
    }
    else{
      this.textHp.GetComponent<TextMeshProUGUI>().SetText(NumberFact.Fact(this.slider.value) + "/" + NumberFact.Fact(this.slider.maxValue));
    }
  }


  public void SetMaxHealth(int health){
    this.slider.maxValue = health;
    this.slider.value = health;

    this.fill.color = gradient.Evaluate(1f);
  }
  public void SetHealth(int health){
    this.slider.value = health;

    this.fill.color = gradient.Evaluate(slider.normalizedValue);
  }

  public void SetMaxShield(int shield){
    this.shieldBar.GetComponent<Slider>().maxValue = shield;
    this.shieldBar.GetComponent<Slider>().value = shield;
  }
  public void SetShield(int shield){
    this.shieldBar.GetComponent<Slider>().value = shield;
  }

  public int GetShield(){
    return (int)(this.shieldBar.GetComponent<Slider>().value);
  }

  public void SubShield(int amount){
    this.shieldBar.GetComponent<Slider>().value -= amount;
  }

  public void AddShield(int amount){
    this.shieldBar.GetComponent<Slider>().value += amount;
  }

  public void SetTextName(string name){
    if (this.textName!=null){
      this.textName.GetComponent<TextMeshProUGUI>().SetText(name);
    }
  }
}
