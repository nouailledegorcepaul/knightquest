﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class WinScreen : MonoBehaviour{

  private GameObject resourcesContainer;
  public GameObject containerTemplate;
  private Animator animator;
  private Board board;

  void Start(){
    this.board = GameObject.Find("Board").GetComponent<Board>();
    this.resourcesContainer = GameObject.Find("Resources");
    this.animator = this.gameObject.GetComponent<Animator>();
  }

  public void ShowWinScreen(Level level, Dictionary<string, int> rewards=null){
    if (rewards==null){
      rewards = new Dictionary<string, int>();
      rewards = level.GetRewards();
    }
    foreach(string element in rewards.Keys){
      if (rewards[element]>0){
        GameObject container = Instantiate(this.containerTemplate, this.transform.position, Quaternion.identity);
        GameObject icon = new GameObject();
        icon.AddComponent<Image>();
        icon.GetComponent<Image>().sprite = Resources.Load<Sprite>("Art/Icons/"+element);
        icon.GetComponent<Image>().preserveAspect = true;
        icon.transform.SetParent(container.transform);
        icon.transform.localScale = new Vector3(1f, 1f, 1f);

        GameObject text = new GameObject();
        text.AddComponent<TextMeshProUGUI>();
        text.GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.MidlineLeft;
        text.GetComponent<TextMeshProUGUI>().text = "+" + rewards[element];
        text.GetComponent<TextMeshProUGUI>().fontSize = 30f;
        text.transform.SetParent(container.transform);
        text.transform.localScale = new Vector3(1f, 1f, 01f);
        container.transform.SetParent(this.resourcesContainer.transform);
        container.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
      }
    }
    this.animator.SetBool("isOpened", true);
  }

  public void ClaimGoToKingdom(){
    StartCoroutine(ClaimToKingdom());
  }

  public IEnumerator ClaimToKingdom(){
    this.animator.SetBool("isOpened", false);
    this.board.transition.SetTrigger("ToKingdom");
    yield return new WaitForSeconds(1f);
    SceneManager.LoadScene("KingdomScene");
  }

}
