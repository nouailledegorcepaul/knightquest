﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataBase{

    public static int id = 0;
    public static Item[] allItems = Resources.LoadAll<Item>("ScriptableObjects/Items");
    public static Item[] headItems = Resources.LoadAll<Item>("ScriptableObjects/Items/Head");
    public static Item[] bodyItems = Resources.LoadAll<Item>("ScriptableObjects/Items/Body");
    public static Item[] pantsItems = Resources.LoadAll<Item>("ScriptableObjects/Items/Pants");
    public static Item[] bootsItems = Resources.LoadAll<Item>("ScriptableObjects/Items/Boots");
    public static Item[] damageItems = Resources.LoadAll<Item>("ScriptableObjects/Items/Damage");
    public static Item[] magicItems = Resources.LoadAll<Item>("ScriptableObjects/Items/Magic");
    public static Item[] shieldItems = Resources.LoadAll<Item>("ScriptableObjects/Items/Shield");
    public static Item[] healItems = Resources.LoadAll<Item>("ScriptableObjects/Items/Heal");

    public static void SetAllId(){
      DataBase.id = DataBase.GetMaxId();;
      int id = DataBase.id;
      foreach (Item item in DataBase.allItems) {
        id = DataBase.id;
        if (item.GetId()==-1){
          item.SetId(id);
          DataBase.id++;
        }
      }
    }

    public static Item GetItemById(int id){
      foreach (Item item in DataBase.allItems){
        if (item.GetId()==id){
          return item;
        }
      }
      return null;
    }

    public static int GetMaxId(){
      int id = 0;
      foreach (Item item in DataBase.allItems) {
        if (item.GetId()>id){
          id = item.GetId();
        }
      }
      id++;
      return id;
    }
}
