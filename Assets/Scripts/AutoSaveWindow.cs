using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AutoSaveWindow : MonoBehaviour{

  public Animator animator;
  public Button yesButton;
  public Button noButton;
  private bool opened = false;
  private Player player;

  void Start(){
    this.player = GameObject.FindWithTag("player").GetComponent<Player>();
  }

  public void Open(){
    this.opened = true;
    this.animator.SetTrigger("Open");
  }

  public void Close(){
    this.opened = false;
    this.animator.SetTrigger("Close");
  }

  public void Yes(){
    this.player.SetAutoSave(true);
    this.player.SavePlayer("auto-" + this.player.GetName());
    Close();
  }

  public void No(){
    this.player.SetAutoSave(false);
    Close();
  }

  public bool IsOpened(){
    return this.opened;
  }
}
