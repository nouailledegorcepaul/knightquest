﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class TutorialWindow : MonoBehaviour{

    public Sprite[] screens;

    [TextArea(3, 10)]
    public string[] screensDescription;

    public TextMeshProUGUI tutorialText;
    public Image tutorialScreen;
    public GameObject previousButton;
    public GameObject nextButton;
    public GameObject continueButton;
    public Animator animator;
    public bool active = false;


    private int index = 0;

    void Awake(){
      Player player = GameObject.FindWithTag("player").GetComponent<Player>();
      if (player.GetCurrentLevel()==0  && SceneManager.GetActiveScene().name=="BoardScene"){
        this.active = true;
        this.animator.SetTrigger("Open");
        UpdateDisplay();
      }
      else if (SceneManager.GetActiveScene().name=="KingdomScene"){
        if ((player.GetCurrentLevel()==1 && !player.SaveTutorialDone()) || !player.SaveTutorialDone()){
          this.active = true;
          this.animator.SetTrigger("Open");
          UpdateDisplay();
        }
      }
      else if (!player.FarmTutorialDone() && SceneManager.GetActiveScene().name=="FarmScene"){
        this.active = true;
        this.animator.SetTrigger("Open");
        UpdateDisplay();
        player.FarmTutorial();
      }
    }

    void Update(){
      this.previousButton.SetActive(this.index>0);
      this.nextButton.SetActive(this.index<this.screens.Length-1);
      this.continueButton.SetActive(this.index==this.screens.Length-1);
    }

    public void SetSprite(Sprite sprite){
      this.tutorialScreen.sprite = sprite;
    }

    public void SetDescription(string text){
      this.tutorialText.text = text;
    }

    public void Previous(){
      this.index--;
      UpdateDisplay();
    }

    public void Next(){
      this.index++;
      UpdateDisplay();
    }

    public void Continue(){
      this.animator.SetTrigger("Close");
      this.active = false;
    }

    public void UpdateDisplay(){
      SetSprite(screens[index]);
      SetDescription(screensDescription[index]);
      tutorialScreen.SetNativeSize();
    }

    public bool IsActive(){
      return this.active;
    }

}
