using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ForgeSlot : MonoBehaviour{

    private ForgeList parent;
    public GameObject image;
    public List<Item> items;
    public Item currentItem;
    private int currentIndex = 0;
    private Player player;
    public GameObject recipeContainer;

    public void Start(){
      this.parent = GameObject.Find("Forge List").GetComponent<ForgeList>();
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
      this.currentItem = items[currentIndex];
      SetSprite(GetSprite(this.currentItem));
      while(this.player.HasCrafted(this.currentItem)){
        this.SetNextItem();
      }
    }

    public List<Item> GetItems(){
      return this.items;
    }

    public void SetNextItem(){
      this.currentItem = null;
      if (this.currentIndex<this.items.Count-1){
        this.currentIndex += 1;
        this.currentItem = this.items[this.currentIndex];
        this.SetSprite(GetSprite(this.currentItem));
      }
      else{
        this.SetSprite(Resources.Load<Sprite>("Art/wip_logo") as Sprite);
      }
    }

    public Sprite GetSprite(Item item){
      return item.GetPrefab().GetComponent<SpriteRenderer>().sprite;
    }

    public void SetSprite(Sprite sprite){
      this.image.GetComponent<Image>().sprite = sprite;
    }

    public Dictionary<string, int> GetRecipe(){
      return this.currentItem.GetRecipe();
    }

    public void Buy(){
      if (this.player.RecipeComplete(this.GetRecipe())){
        this.player.Craft(this.GetRecipe());
        this.player.AddInventory(this.currentItem);
        this.player.SetItem(this.currentItem);
        SetNextItem();
        this.parent.ClearCraftContainer();
        this.parent.SetButtonActive(false);
      }
    }

    public void DisplayRecipe(){
      this.parent.ClearCraftContainer();
      if (this.currentItem!=null){
        if (this.GetRecipe()!=null){
          if (!this.player.HasCrafted(this.currentItem)){
            GameObject container = GameObject.Find("Resources Needed");
            List<GameObject> recipeList = new List<GameObject>();
            foreach(string element in this.GetRecipe().Keys){
              if (element!="gold" && this.GetRecipe()[element]>0){
                GameObject elem = Instantiate(this.recipeContainer, container.transform.position, Quaternion.identity);
                elem.name = element + "container";
                // GameObject elem = new GameObject(element + " container");
                // elem.AddComponent<VerticalLayoutGroup>();
                GameObject elemImage = new GameObject(element + " image");
                elemImage.AddComponent<Image>();
                elemImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("Art/Icons/"+element);
                elemImage.GetComponent<Image>().preserveAspect = true;
                GameObject amount = new GameObject(element + " amount");
                amount.AddComponent<TextMeshProUGUI>();
                amount.GetComponent<TextMeshProUGUI>().SetText(NumberFact.Fact(this.player.GetResources()[element]) + "/" + NumberFact.Fact(this.GetRecipe()[element]));
                amount.GetComponent<TextMeshProUGUI>().fontSize = 20f;

                elemImage.transform.SetParent(elem.transform);
                amount.transform.SetParent(elem.transform);
                elem.transform.SetParent(container.transform);
                recipeList.Add(elem);
              }
            }
            for (int i = 0; i<recipeList.Count; i++){
              if (recipeList[i]!=null){
                recipeList[i].transform.localScale = new Vector3(2f, 2f, 2f)/recipeList.Count*1.3f;
                if (recipeList.Count==1){
                  recipeList[i].transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                }
              }
            }
            if (this.player.RecipeComplete(this.GetRecipe())){
              this.parent.SetButtonColor("green");
              this.parent.SetButtonInteractable(true);
            }
            else{
              this.parent.SetButtonColor("red");
              this.parent.SetButtonInteractable(false);
            }
            this.parent.SetButtonText(this.GetRecipe()["gold"] + "g");
            this.parent.GetForgeButton().GetComponent<Button>().onClick.RemoveAllListeners();
            this.parent.GetForgeButton().GetComponent<Button>().onClick.AddListener(Buy);
          }
        }
        this.parent.SetButtonActive(this.GetRecipe()!=null);
      }
    }
}
