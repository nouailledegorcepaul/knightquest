﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuitWindow : MonoBehaviour{

    public Animator animator;
    public Button yesButton;
    public Button noButton;

    public void Open(){
      this.animator.SetTrigger("Open");
    }

    public void Close(){
      this.animator.SetTrigger("Close");
    }

    public void Quit(){
      SceneManager.LoadScene("MainMenuScene");
    }
}
