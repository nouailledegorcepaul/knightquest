﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Locker : MonoBehaviour{

    public Sprite locked;
    public Sprite unlocked;

    public Board board;

    // Update is called once per frame
    void Update(){
      if (board.IsLocked()){
        this.GetComponent<Image>().sprite = locked;
      }
      else{
        this.GetComponent<Image>().sprite = unlocked;
      }
    }
}
