using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class FarmSlot : MonoBehaviour{

  public GameObject containerTemplate;
  public Player player;
  public World world;
  public Animator transition;
  public GameObject difficultyPopUp;
  private bool spamProof = false;

  void Start(){
    this.player = GameObject.FindWithTag("player").GetComponent<Player>();
    this.difficultyPopUp = GameObject.Find("Difficulty Pop Up");
    this.difficultyPopUp.GetComponentInChildren<Button>().onClick.AddListener(() => GoToLevel());
    foreach (Level farmLevel in this.world.GetFarmLevels()){
      string category = farmLevel.GetCategory();
      string name = farmLevel.GetName();
      GameObject mainContainer = Instantiate(this.containerTemplate, this.transform.position, Quaternion.identity);
      mainContainer.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = name;
      mainContainer.transform.GetChild(1).GetChild(2).GetComponent<Button>().onClick.AddListener(() => SetLevel(farmLevel));
      mainContainer.transform.GetChild(1).GetChild(2).GetComponent<Button>().interactable = player.TestFarmCategorieUnlocked(category);

      Sprite sprite = Resources.Load<Sprite>("Art/Farm/" + category + "_background") as Sprite;
      if (sprite==null){
        sprite = Resources.Load<Sprite>("Art/wip_logo") as Sprite;
      }

      mainContainer.transform.GetChild(1).GetChild(1).GetComponentInChildren<Image>().sprite = sprite;
      mainContainer.transform.SetParent(this.gameObject.transform, false);
    }
  }

  void Update(){
    if (Input.GetKeyDown(KeyCode.Escape)){
      SceneManager.LoadScene("KingdomScene");
    }
  }

  public void SetLevel(Level level){
    if (!this.spamProof){
      this.spamProof = true;
      string difficulty = "easy";
      int percentage = Random.Range(1, 100);
      if (percentage < 5){
        difficulty = "hard";
      }
      else if (percentage < 20){
        difficulty = "medium";
      }
      level.SetFarmLevelDifficulty(this.player, difficulty);
      SetPopUpInformations(level, difficulty);
      this.player.SetLevel(level);
      this.difficultyPopUp.GetComponent<Animator>().SetTrigger("Open");
    }
  }

  public void GoToLevel(){
    StartCoroutine(LevelTransition());
  }

  public IEnumerator LevelTransition(){
    this.transition.SetTrigger("Play");
    yield return new WaitForSeconds(0.1f);
    this.difficultyPopUp.GetComponent<Animator>().SetTrigger("Close");
    yield return new WaitForSeconds(0.8f);
    SceneManager.LoadScene("BoardScene");
  }

  public void SetPopUpInformations(Level level, string difficulty){
    GameObject.Find("Level Infos").GetComponent<TextMeshProUGUI>().text = level.GetDifficultyText(difficulty);
  }

}
