using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopUpManager : MonoBehaviour{

  public List<string> informations;
  public GameObject previousButton;
  public GameObject continueButton;
  public GameObject nextButton;
  public Animator animator;
  public TextMeshProUGUI text;
  private int index;

  void Start(){
    this.informations = new List<string>();
    this.index = 0;
  }

  void Update(){
    this.previousButton.SetActive(this.index>0);
    this.nextButton.SetActive(this.index<this.informations.Count-1);
    this.continueButton.SetActive(this.index==this.informations.Count-1);
  }

  public void AddInfos(Player player){
    this.informations.Add("Level up!\nYou are now level " + player.GetExpLevel().ToString());
  }

  public void AddInfos(Level level){
    this.informations.Add("New farm level unlocked\n" + level.GetName().ToString());
  }

  public void Open(){
    if (this.informations.Count>0){
      this.text.text = this.informations[0];
      this.animator.SetTrigger("Open");
    }
  }

  public void Previous(){
    this.index--;
    this.text.text = this.informations[this.index];
  }

  public void Continue(){
    this.index = 0;
    this.informations.Clear();
    this.animator.SetTrigger("Close");
  }

  public void Next(){
    this.index++;
    this.text.text = this.informations[this.index];
  }
}
