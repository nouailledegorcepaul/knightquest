﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour{

    private Player player;
    public Animator animator;

    void Start(){
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
      this.animator.SetTrigger("FadeOut");
      if (this.player.GetCurrentLevel()==0){
        StartCoroutine(DisplayStartTutorial());
      }
      else{
        StartCoroutine(DisplayEndTutorial());
      }

    }

    public IEnumerator DisplayStartTutorial(){
      yield return new WaitForSeconds(2f);
      Dialogue dialogue = this.player.GetWorld().GetLevels()[0].GetStartDialogue();
      this.player.GetDialogueBox().StartDialogue(dialogue, true);
      StartCoroutine(DialoguePriority(false));
    }


    public IEnumerator DisplayEndTutorial(){
      yield return new WaitForSeconds(2f);
      Dialogue dialogue = this.player.GetWorld().GetLevels()[0].GetEndDialogue();
      this.player.GetDialogueBox().StartDialogue(dialogue, true);
      StartCoroutine(DialoguePriority(true));
    }

    public IEnumerator DialoguePriority(bool tutorialEnded){
      while(!this.player.GetDialogueBox().DialogueEnded()){
        yield return null;
      }
      this.animator.SetTrigger("FadeIn");
      if (tutorialEnded){
        this.player.GetDialogueBox().TutorialModSwap();
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("KingdomScene");
      }
      else{
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("BoardScene");
      }
    }

}
