﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerNameInput : MonoBehaviour{

    public TextMeshProUGUI inputText;
    public TextMeshProUGUI confirmMessage;
    private Player player;
    private bool clicked = false;
    public Animator animator;
    public Animator confirmContainerAnimator;
    private string playerName = "";
    public TMP_InputField inputField;

    void Start(){
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
      this.inputField.characterLimit = 15;
    }

    void Update(){
      if (Input.GetKeyDown(KeyCode.Return)){
        ConfirmInput();
      }
    }

    public void ConfirmInput(){
      if (!this.clicked){
        this.clicked = true;
        this.playerName = "";
        for (int i=0; i<this.inputText.text.Length-1;i++){
          this.playerName+= this.inputText.text.ToCharArray()[i];
        }
        if (this.playerName==""){
          this.playerName = "Mint";
        }
        this.confirmMessage.text = "Your name is:\n"+this.playerName;
        this.confirmContainerAnimator.SetTrigger("Open");
      }
    }

    public void CloseConfirmContainer(){
      this.confirmContainerAnimator.SetTrigger("Close");
      this.clicked = false;
    }

    public void StartGame(){
      this.player.SetPlayerName(this.playerName);
      this.player.Init();
      StartCoroutine(ProfileTransition());
    }

    public IEnumerator ProfileTransition(){
      this.CloseConfirmContainer();
      this.clicked = true;
      this.animator.SetTrigger("Confirm");
      yield return new WaitForSeconds(1f);
      SceneManager.LoadScene("TutorialScene");
    }
}
