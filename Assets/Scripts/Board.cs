using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public enum GameState{
  wait,
  move
}

public class Board : MonoBehaviour{

  [Header("Dimensions")]
  public int width;
  public int height;
  public int offSet;

  [Header("Prefabs")]
  public GameObject backgroundPrefab;
  private GameObject[] elementPrefabs;
  public GameObject rockPrefab;
  public GameObject columnPowerUpPrefab;
  public GameObject rowPowerUpPrefab;
  public GameObject bombPowerUpPrefab;
  public GameObject superPowerUpPrefab;

  [Header("Current Elements")]
  public GameObject[,] elements;
  private GameObject swapElement1;
  private GameObject swapElement2;

  [Header("Player variables")]
  public Player player;
  public GameObject playerHealthBar;
  public GameObject playerPotionCharge;
  public GameObject playerShieldBar;
  public GameObject playerGoldText;
  public GameObject playerMoves;
  public GameObject playerScore;
  public GameObject playerDamage;
  private int moves = 0;
  private int saveDamage = 0;
  private int damage = 0;
  private int damageToDeal = 0;
  private int magicToDeal = 0;

  [Header("Enemy variables")]
  public Enemy enemy;
  public GameObject enemyHealthBar;
  public GameObject enemyDefenseIcon;
  public GameObject enemyMagicDefenseIcon;
  public GameObject enemyDamageIcon;
  public GameObject enemyMagicIcon;

  [Header("Others")]
  public Level level;
  public int currentLevel;
  private int currentMoves;
  public GameState state;
  public bool ready = false;
  public bool locked = false;
  private bool dialoguePrio = false;
  public int powerUpCurrentlyFired = 0;
  private Dictionary<string, int> farmLevelOnHitResources;
  private int score;
  private bool antiSpam = false;
  private bool animDamage = false;
  private bool rewardsTaken = false;
  private bool enemyCanAttack = true;
  public bool playerTurn = true;
  private bool dialogueDone = false;

  public TextMeshProUGUI damageTaken;
  public Animator transition;
  public Animator loseScreen;
  public WinScreen winScreen;

  void Start(){
    this.player = GameObject.FindWithTag("player").GetComponent<Player>();
    this.player.InitBars(this.playerHealthBar, this.playerShieldBar, this.playerPotionCharge);
    StartCoroutine(InitializeGameWithFade());
    InitGame();
  }

  void Update(){
    if (this.level.GetMoves()!=-1){
      this.playerDamage.SetActive(false);
      this.playerMoves.GetComponent<TextMeshProUGUI>().text = "Moves: " + (this.level.GetMoves()-this.moves);
      this.playerScore.GetComponent<TextMeshProUGUI>().text = "<b>Score</b>: " + this.score;
      if (this.score<600){
        this.playerScore.GetComponent<TextMeshProUGUI>().color = new Color32(171, 32, 22, 255);
      }
      else if (this.score>=600 && this.score<1000){
        this.playerScore.GetComponent<TextMeshProUGUI>().color = new Color32(203, 215, 27, 255);
      }
      else{
        this.playerScore.GetComponent<TextMeshProUGUI>().color = new Color32(17, 121, 15, 255);
      }
    }
    else{
      this.playerMoves.SetActive(false);
      this.playerScore.SetActive(false);
    }
    this.playerDamage.GetComponent<TextMeshProUGUI>().text = "" + this.saveDamage;
    if (!this.ready){
      this.state = GameState.wait;
    }
    if (!this.antiSpam && this.ready){
      this.antiSpam = true;
      StartCoroutine(DamageDisplay());
    }
  }

  public void InitGame(bool restart = false){
    this.damageTaken.GetComponent<TextMeshProUGUI>().enabled = false;
    this.elementPrefabs = new GameObject[5];
    this.elementPrefabs[0] = this.player.GetDamageItem().GetPrefab();
    this.elementPrefabs[1] = this.player.GetMagicItem().GetPrefab();
    this.elementPrefabs[2] = this.player.GetShieldItem().GetPrefab();
    this.elementPrefabs[3] = this.player.GetHealItem().GetPrefab();
    this.elementPrefabs[4] = this.rockPrefab;

    this.elements = new GameObject[this.width, this.height];
    for (int i = 0; i<this.width; i++){
      for (int j = 0; j<this.height; j++){
        Vector2 pos = new Vector2(i, j);
        //Instantiation of backgrounds
        GameObject background = Instantiate(this.backgroundPrefab, pos, Quaternion.identity) as GameObject;
        background.transform.parent = this.transform;
        background.name = "(" + i + ", " + j + ")";
        //Instantiation of element at column i, row j
        int trials = 0;
        int randomRock = Random.Range(0, 100);
        int randomElementIndex = Random.Range(0, this.elementPrefabs.Length-1);
        if (randomRock<15 && this.GetRockAmount()<8){
          randomElementIndex = 4;
        }
        AddElementAt(i, j, this.elementPrefabs[randomElementIndex]);
        //Instantiation of non-matched elements
        MatchElements();
        while (HasMatches() && trials<50){
          Unmatches();
          DestroyElementAt(i, j);
          randomElementIndex = Random.Range(0, this.elementPrefabs.Length);
          AddElementAt(i, j, this.elementPrefabs[randomElementIndex]);
          MatchElements();
          trials++;
        }
      }
    }
    if (!restart){
      this.score = 0;
      this.level = this.player.GetLevel();
      this.enemy = this.level.GetEnemy();
      this.enemy.InitBars(this.enemyHealthBar);
      if (this.level.GetCategory()=="main" || this.level.GetCategory()=="tutorial"){
        this.enemyHealthBar.SetActive(true);
      }
      else{
        this.enemyHealthBar.transform.GetChild(0).gameObject.SetActive(false);
        this.enemyHealthBar.transform.GetChild(1).gameObject.SetActive(false);
        this.enemyHealthBar.transform.GetChild(2).gameObject.SetActive(false);
      }
      this.enemyDefenseIcon.SetActive(this.enemy.GetDefense()>0);
      this.enemyMagicDefenseIcon.SetActive(this.enemy.GetMagicDefense()>0);
      this.enemyDamageIcon.SetActive(this.enemy.GetDamage()>0);
      this.enemyMagicIcon.SetActive(this.enemy.GetMagic()>0);
      this.player.Reset();
      this.moves = 0;
      if (this.level.GetCategory()=="tutorial"){
        StartCoroutine(TutorialPriority());
      }
    }
    if (!CheckMatchPossible()){
      this.ready = false;
      ClearElements();
      InitGame(true);
    }
    else{
      if (this.level.GetStartDialogue()!=null && this.level.GetCategory()!="tutorial" && !this.dialogueDone){
        this.dialogueDone = true;
        this.player.GetDialogueBox().StartDialogue(this.level.GetStartDialogue());
        StartCoroutine(DialoguePriority());
      }
      else{
        this.state = GameState.move;
      }
      this.ready = true;
    }
  }

  public IEnumerator DamageDisplay(){
    if (this.state == GameState.wait || !this.playerTurn){
      while(this.damage>0){
        this.animDamage = true;
        if (this.damage<10){
          this.saveDamage += 1;
          this.damage -= 1;
        }
        else if (this.damage>=10 && this.damage<100){
          this.saveDamage += 10;
          this.damage -= 10;
        }
        else if (this.damage>=100 && this.damage<1000){
          this.saveDamage += 100;
          this.damage -= 100;
        }
        else if (this.damage>=1000 && this.damage<10000){
          this.saveDamage += 1000;
          this.damage -= 1000;
        }
        else{
          this.saveDamage += 10000;
          this.damage -= 10000;
        }
        yield return new WaitForSeconds(0.05f);
      }
    }
    if (this.saveDamage>0){
      int d1 = this.damageToDeal;
      int d2 = this.magicToDeal;
      this.damageToDeal = 0;
      this.magicToDeal = 0;

      this.playerDamage.GetComponent<Animator>().SetTrigger("Attack");
      yield return new WaitForSeconds(0.4f);
      this.saveDamage = 0;
      this.player.Attack(d1, "damage", this.enemy);
      this.player.Attack(d2, "magic", this.enemy);

      this.animDamage = false;
    }
    if (!this.enemy.IsAlive()){
      this.state = GameState.wait;
      this.ready = false;
      if (this.level.GetEndDialogue()!=null){
        this.player.GetDialogueBox().StartDialogue(this.level.GetEndDialogue());
        StartCoroutine(DialoguePriority());
      }
      StartCoroutine(ShowWinScreen());
    }
    yield return new WaitForSeconds(0.1f);
    this.antiSpam = false;
  }

  public IEnumerator DialoguePriority(){
    this.dialoguePrio = true;
    while(!this.player.GetDialogueBox().DialogueEnded()){
      this.state = GameState.wait;
      yield return null;
    }
    this.dialoguePrio = false;
    this.state = GameState.move;
  }

  public IEnumerator TutorialPriority(){
    TutorialWindow tw = GameObject.Find("Tutorial Explanations").GetComponent<TutorialWindow>();
    while(tw.IsActive()){
      this.state = GameState.wait;
      yield return null;
    }
    this.state = GameState.move;
  }

  public void Restart(bool noMatches = false){
    StartCoroutine(RestartLevel(noMatches));
  }

  public IEnumerator RestartLevel(bool noMatches){
    this.ready = false;
    this.state = GameState.wait;
    this.loseScreen.SetTrigger("Close");
    this.transition.SetTrigger("Restart");
    yield return new WaitForSeconds(0.5f);
    ClearElements();
    InitGame(noMatches);
  }

  public IEnumerator InitializeGameWithFade(){
    this.transition.SetTrigger("InitGame");
    yield return new WaitForSeconds(1f);
  }

  public int GetWidth(){return this.width;}
  public int GetHeight(){return this.height;}
  public GameObject[,] GetElements(){return this.elements;}
  public GameObject[] GetElementPrefabs(){return this.elementPrefabs;}
  public GameState GetState(){return this.state;}
  public bool IsLocked(){return this.locked;}
  public void Lock(){this.locked = true;}
  public void Unlock(){this.locked = false;}
  public void SetState(GameState state){this.state = state;}
  public void SetEnemy(Enemy enemy){this.enemy = enemy;}
  public void AddDamage(int damage){this.damage = damage;}

  public IEnumerator SwapElements(string direction, Element elem1, Element elem2){
    this.swapElement1 = elem1.gameObject;
    this.swapElement2 = elem2.gameObject;
    string tag1 = this.swapElement1.tag;
    string tag2 = this.swapElement2.tag;
    bool oneSuperPowerUp = tag1=="super_power-up" || tag2=="super_power-up";
    bool twoSuperPowerUp = tag1=="super_power-up" && tag2=="super_power-up";

    bool oneBombPowerUp = tag1=="bomb_power-up" || tag2=="bomb_power-up";
    bool twoBombPowerUp = tag1=="bomb_power-up" && tag2=="bomb_power-up";

    bool twoPowerUp = (tag1=="row_power-up" || tag1=="column_power-up") && (tag2=="row_power-up" || tag2=="column_power-up");
    bool onePowerUp = tag1=="row_power-up" || tag1=="column_power-up" || tag2=="row_power-up" || tag2=="column_power-up";

    bool globalTwoPowerUp = twoSuperPowerUp || twoBombPowerUp || twoPowerUp ||
                            (oneSuperPowerUp && oneBombPowerUp) || (oneSuperPowerUp && onePowerUp) || (oneBombPowerUp && onePowerUp);

    this.state = GameState.wait;
    if (direction == "right" || direction == "left"){
      if (direction == "right"){
        elem1.IncrementColumn();
        if (!globalTwoPowerUp){
          elem2.DecrementColumn();
        }
      }
      else{
        elem1.DecrementColumn();
        if (!globalTwoPowerUp){
          elem2.IncrementColumn();
        }
      }
    }
    else{
      if (direction == "up"){
        elem1.IncrementRow();
        if (!globalTwoPowerUp){
          elem2.DecrementRow();
        }
      }
      else{
        elem1.DecrementRow();
        if (!globalTwoPowerUp){
          elem2.IncrementRow();
        }
      }
    }

    this.elements[elem1.GetColumn(), elem1.GetRow()] = elem1.gameObject;
    this.elements[elem2.GetColumn(), elem2.GetRow()] = elem2.gameObject;

    yield return new WaitForSeconds(0.25f);
    MatchElements();
    if (twoSuperPowerUp){
      ActivateSuperPowerUp(this.swapElement1, this.swapElement2);
    }
    else if (oneSuperPowerUp){
      if (tag1=="super_power-up"){
        ActivateSuperPowerUp(this.swapElement1, this.swapElement2);
      }
      else{
        ActivateSuperPowerUp(this.swapElement2, this.swapElement1);
      }
    }
    else if (twoBombPowerUp){
      ActivateDoubleBombPowerUp(this.swapElement1, this.swapElement2);
    }
    else if (oneBombPowerUp && onePowerUp){
      if (tag1=="bomb_power-up"){
        ActivateBombAndPowerUp(this.swapElement1, this.swapElement2);
      }
      else{
        ActivateBombAndPowerUp(this.swapElement2, this.swapElement2);
      }
    }
    else if (oneBombPowerUp){
      if (tag1=="bomb_power-up"){
        ActivateBombPowerUp(this.swapElement1);
      }
      else{
        ActivateBombPowerUp(this.swapElement2);
      }
    }
    else if (twoPowerUp){
      ActivatePowerUp(this.swapElement1, this.swapElement2);
    }
    else if (onePowerUp){
      if (tag1=="row_power-up" || tag1=="column_power-up"){
        ActivatePowerUp(this.swapElement1);
      }
      else{
        ActivatePowerUp(this.swapElement2);
      }
    }

    if (!HasMatches() && !onePowerUp && !oneBombPowerUp && !oneSuperPowerUp){
      SwapBack(direction, elem2, elem1);
      yield return new WaitForSeconds(0.5f);
      this.state = GameState.move;
    }
    else{
      playerTurn = false;
      this.moves++;
      DestroyMatches();
    }
  }

  public void SwapBack(string direction, Element elem1, Element elem2){
    if (direction == "right" || direction == "left"){
      if (direction == "right"){
        elem1.IncrementColumn();
        elem2.DecrementColumn();
      }
      else{
        elem1.DecrementColumn();
        elem2.IncrementColumn();
      }
    }
    else{
      if (direction == "up"){
        elem1.IncrementRow();
        elem2.DecrementRow();
      }
      else{
        elem1.DecrementRow();
        elem2.IncrementRow();
      }
    }
    this.elements[elem1.GetColumn(), elem1.GetRow()] = elem1.gameObject;
    this.elements[elem2.GetColumn(), elem2.GetRow()] = elem2.gameObject;
  }

  public void ActivatePowerUp(GameObject pu1, GameObject pu2=null){
    List<GameObject> powerUps = new List<GameObject>(){pu1, pu2};

    GameObject prefab;
    int x, y, x1, x2, y1, y2;
    string tag = pu1.tag;
    for (int i = 0; i<powerUps.Count; i++){
      if (powerUps[i]!=null){
        powerUps[i].GetComponent<Element>().PowerUpUsed();
        x = powerUps[i].GetComponent<Element>().GetColumn();
        y = powerUps[i].GetComponent<Element>().GetRow();

        prefab = this.columnPowerUpPrefab;
        x1 = x;
        y1 = this.height;
        x2 = x;
        y2 = -1;
        if (tag=="row_power-up"){
          prefab = this.rowPowerUpPrefab;
          x1 = this.width;
          y1 = y;
          x2 = -1;
          y2 = y;
        }

        Destroy(powerUps[i]);
        this.elements[x, y] = null;

        Vector2 pos = new Vector2(x, y);

        GameObject powerUp1 = Instantiate(prefab, pos, Quaternion.identity);
        powerUp1.GetComponent<Element>().SetColumn(x1);
        powerUp1.GetComponent<Element>().SetRow(y1);
        powerUp1.transform.parent = this.transform;

        GameObject powerUp2 = Instantiate(prefab, pos, Quaternion.identity);
        powerUp2.GetComponent<Element>().SetColumn(x2);
        powerUp2.GetComponent<Element>().SetRow(y2);
        powerUp2.transform.parent = this.transform;

        this.powerUpCurrentlyFired+=2;

        if (tag=="row_power-up"){
          tag = "column_power-up";
        }
        else{
          tag="row_power-up";
        }
      }
    }
  }

  public void ActivateDoubleBombPowerUp(GameObject pu1, GameObject pu2){
    List<GameObject> powerUps = new List<GameObject>(){pu1, pu2};

    pu1.GetComponent<Element>().PowerUpUsed();
    pu2.GetComponent<Element>().PowerUpUsed();

    int x = pu1.GetComponent<Element>().GetColumn();
    int y = pu1.GetComponent<Element>().GetRow();
    int x2 = pu2.GetComponent<Element>().GetColumn();
    int y2 = pu2.GetComponent<Element>().GetRow();

    for (int i = x-2; i<x+3; i++){
      if (i>=0 && i<this.width){
        for (int j = y-2; j<y+3; j++){
          if (j>=0 && j<this.height){
            if (this.elements[i, j]!=null){
              this.elements[i, j].GetComponent<Element>().Match();
            }
          }
        }
      }
    }
  }

  public void ActivateBombPowerUp(GameObject pu1, GameObject pu2=null){
    List<GameObject> powerUps = new List<GameObject>(){pu1, pu2};
    int x = pu1.GetComponent<Element>().GetColumn();
    int y = pu1.GetComponent<Element>().GetRow();

    pu1.GetComponent<Element>().PowerUpUsed();

    if (pu2==null){
      for (int i = x-1; i<x+2; i++){
        if (i>=0 && i<this.width){
          for (int j = y-1; j<y+2; j++){
            if (j>=0 && j<this.height){
              if (this.elements[i, j]!=null){
                this.elements[i, j].GetComponent<Element>().Match();
              }
            }
          }
        }
      }
      if (x-2>=0 && this.elements[x-2,y]!=null){
        this.elements[x-2,y].GetComponent<Element>().Match();
      }
      if (x+2<this.width && this.elements[x+2, y]!=null){
        this.elements[x+2, y].GetComponent<Element>().Match();
      }
      if (y-2>=0 && this.elements[x,y-2]!=null){
        this.elements[x,y-2].GetComponent<Element>().Match();
      }
      if (y+2<this.height && this.elements[x, y+2]!=null){
        this.elements[x, y+2].GetComponent<Element>().Match();
      }
      DestroyMatches();
    }
  }

  public void ActivateBombAndPowerUp(GameObject pu1, GameObject pu2){
    List<GameObject> powerUps = new List<GameObject>(){pu1, pu2};
    int x = pu1.GetComponent<Element>().GetColumn();
    int y = pu1.GetComponent<Element>().GetRow();
    int x2 = pu2.GetComponent<Element>().GetColumn();
    int y2 = pu2.GetComponent<Element>().GetRow();

    pu1.GetComponent<Element>().PowerUpUsed();
    pu2.GetComponent<Element>().PowerUpUsed();

    pu1.GetComponent<Element>().Match();
    pu2.GetComponent<Element>().Match();
    Destroy(powerUps[0]);
    this.elements[x, y] = null;
    Destroy(powerUps[1]);
    this.elements[x2, y2] = null;

    GameObject prefab = this.columnPowerUpPrefab;
    for (int i = x-1; i<x+2; i++){
      if (i>=0 && i<this.width){
        Vector2 pos = new Vector2(i, y);

        if (this.elements[i, y]!=null){
          DestroyElementAt(i, y);
          this.elements[i, y] = null;
        }

        GameObject powerUp1 = Instantiate(prefab, pos, Quaternion.identity);
        powerUp1.GetComponent<Element>().SetColumn(i);
        powerUp1.GetComponent<Element>().SetRow(this.height);
        powerUp1.transform.parent = this.transform;

        GameObject powerUp2 = Instantiate(prefab, pos, Quaternion.identity);
        powerUp2.GetComponent<Element>().SetColumn(i);
        powerUp2.GetComponent<Element>().SetRow(-1);
        powerUp2.transform.parent = this.transform;

        this.powerUpCurrentlyFired+=2;
      }
    }

    prefab = this.rowPowerUpPrefab;
    for (int i = y-1; i<y+2; i++){
      if (i>=0 && i<this.height){
        Vector2 pos = new Vector2(x, i);

        if (this.elements[x, i]!=null){
          DestroyElementAt(x, i);
          this.elements[x, i] = null;
        }

        GameObject powerUp1 = Instantiate(prefab, pos, Quaternion.identity);
        powerUp1.GetComponent<Element>().SetColumn(this.width);
        powerUp1.GetComponent<Element>().SetRow(i);
        powerUp1.transform.parent = this.transform;

        GameObject powerUp2 = Instantiate(prefab, pos, Quaternion.identity);
        powerUp2.GetComponent<Element>().SetColumn(-1);
        powerUp2.GetComponent<Element>().SetRow(i);
        powerUp2.transform.parent = this.transform;

        this.powerUpCurrentlyFired+=2;
      }
    }
    DestroyMatches();
  }

  public void ActivateSuperPowerUp(GameObject pu1, GameObject pu2=null){
    List<GameObject> powerUps = new List<GameObject>(){pu1, pu2};
    if (pu2!=null){
      pu1.GetComponent<Element>().PowerUpUsed();
      pu1.GetComponent<Element>().Match();

      int x = pu1.GetComponent<Element>().GetColumn();
      int y = pu1.GetComponent<Element>().GetRow();

      int x2 = pu2.GetComponent<Element>().GetColumn();
      int y2 = pu2.GetComponent<Element>().GetRow();
      string tag = pu2.tag;

      pu2.GetComponent<Element>().PowerUpUsed();
      pu2.GetComponent<Element>().Match();

      if (tag=="damage" || tag=="magic" || tag=="shield" || tag=="heal"){
        for (int i = 0; i<this.width; i++){
          for (int j = 0; j<this.height; j++){
            if (this.elements[i, j]!=null){
              string tagElem = this.elements[i, j].tag;
              if (tagElem==tag){
                this.elements[i, j].GetComponent<Element>().Match();
              }
            }
          }
        }
        DestroyMatches();
      }
      // With Fire Ball Power Up
      else if (tag=="row_power-up" || tag=="column_power-up"){
        int amount = Random.Range(4, 7);
        List<Vector2> posToActivate = new List<Vector2>();
        while(amount>0){
          for (int i = 0; i<this.width; i++){
            for (int j = 0; j<this.height; j++){
              if (i!=x && j!=y && i!=x2 && j!=y2){
                if (this.elements[i, j]!=null){
                  string tagElem = this.elements[i, j].tag;
                  if (tagElem=="damage" || tagElem=="magic" || tagElem=="heal" || tagElem=="shield"){
                    int percentage = Random.Range(0, 100);
                    if (percentage<5 && amount>0){
                      amount--;
                      Vector2 pos = new Vector2(i, j);
                      int percentageDirection = Random.Range(0,2);
                      GameObject prefab = null;
                      if (percentageDirection==0){
                        prefab = this.columnPowerUpPrefab;
                      }
                      else{
                        prefab = this.rowPowerUpPrefab;
                      }
                      Destroy(this.elements[i, j]);
                      this.elements[i, j]=null;
                      AddElementAt(i, j, prefab);
                      posToActivate.Add(pos);
                    }
                  }
                }
              }
            }
          }
        }
        foreach(Vector2 pos in posToActivate){
          if (this.elements[(int)(pos[0]), (int)(pos[1])]!=null){
            ActivatePowerUp(this.elements[(int)(pos[0]), (int)(pos[1])]);
          }
        }
      }
      // With Bomb Power Up
      else if (tag=="bomb_power-up"){
        int amount = Random.Range(3, 5);
        List<Vector2> posToActivate = new List<Vector2>();
        while(amount>0){
          for (int i = 0; i<this.width; i++){
            for (int j = 0; j<this.height; j++){
              if (i!=x && j!=y && i!=x2 && j!=y2){
                if (this.elements[i, j]!=null){
                  string tagElem = this.elements[i, j].tag;
                  if (tagElem=="damage" || tagElem=="magic" || tagElem=="heal" || tagElem=="shield"){
                    int percentage = Random.Range(0, 100);
                    if (percentage<5 && amount>0){
                      amount--;
                      Vector2 pos = new Vector2(i, j);
                      GameObject prefab = this.bombPowerUpPrefab;

                      Destroy(this.elements[i, j]);
                      this.elements[i, j]=null;
                      AddElementAt(i, j, prefab);
                      posToActivate.Add(pos);
                    }
                  }
                }
              }
            }
          }
        }
        foreach(Vector2 pos in posToActivate){
          if (this.elements[(int)(pos[0]), (int)(pos[1])]!=null){
            this.powerUpCurrentlyFired++;
            ActivateBombPowerUp(this.elements[(int)(pos[0]), (int)(pos[1])]);
            this.powerUpCurrentlyFired--;
          }
        }
      }
      else if (tag=="super_power-up"){
        for (int i = 0; i<this.width; i++){
          for (int j = 0; j<this.height; j++){
            if (this.elements[i, j]!=null){
              DestroyElementAt(i, j);
            }
          }
        }
      }
    }
    // Normal Super Power Up with elements
    else{
      bool found = false;
      while (!found){
        for (int i = 0; i<this.width; i++){
          for (int j = 0; j<this.height; j++){
            if (!found){
              if (this.elements[i, j]!=null){
                string tagElem = this.elements[i, j].tag;
                if (tagElem=="damage" || tagElem=="magic" || tagElem=="heal" || tagElem=="shield"){
                  if (Random.Range(0, 10)<2){
                    found = true;
                    ActivateSuperPowerUp(pu1, this.elements[i, j]);
                    break;
                  }
                }
              }
            }
          }
        }
      }
    }
  }

//[----------------------- MATCH FUNCTIONS -----------------------]

  public bool CheckMatchPossible(){
    for (int i = 0; i<this.width-1; i++){
      for (int j = 0; j<this.height; j++){
        if (this.elements[i, j]!=null && this.elements[i+1, j]!=null){
          GameObject go1 = this.elements[i, j];
          GameObject go2 = this.elements[i+1, j];
          if (go1.tag!="rock" && go2.tag!="rock"){
            this.elements[i+1, j] = go1;
            this.elements[i, j] = go2;
            if (CanMatchElements()){
              this.elements[i, j] = go1;
              this.elements[i+1, j] = go2;
              return true;
            }
            this.elements[i, j] = go1;
            this.elements[i+1, j] = go2;
          }
        }
      }
    }
    for (int j = 0; j<this.height-1; j++){
      for (int i = 0; i<this.width; i++){
        if (this.elements[i, j]!=null && this.elements[i, j+1]!=null){
          GameObject go1 = this.elements[i, j];
          GameObject go2 = this.elements[i, j+1];
          if (go1.tag!="rock" && go2.tag!="rock"){
            this.elements[i, j+1] = go1;
            this.elements[i, j] = go2;
            if (CanMatchElements()){
              this.elements[i, j] = go1;
              this.elements[i, j+1] = go2;
              return true;
            }
            this.elements[i, j] = go1;
            this.elements[i, j+1] = go2;
          }
        }
      }
    }
    return false;
  }

  public bool CanMatchElements(){
    List<GameObject> matchedElements = new List<GameObject>();
    string tag = "";
    for (int i = 0; i < this.width; i++){
      if (this.elements[i, 0]!=null){
        tag = this.elements[i, 0].tag;
        for (int j = 0; j < this.height; j++){
          if (tag!="rock"){
            if (tag=="row_power-up" || tag=="column_power-up" || tag=="super_power-up" || tag=="bomb_power-up"){
              return true;
            }
            if (this.elements[i, j]!=null){
              if (this.elements[i, j].tag == tag){
                matchedElements.Add(this.elements[i, j]);
              }
              else{
                if (matchedElements.Count>2){
                  return true;
                }
                tag = this.elements[i, j].tag;
                matchedElements.Clear();
                matchedElements.Add(this.elements[i, j]);
              }
            }
          }
        }
        if (matchedElements.Count>2){
          return true;
        }
        matchedElements.Clear();
      }
    }
    tag = "";
    for (int j = 0; j < this.height; j++){
      if (this.elements[0, j]!=null){
        tag = this.elements[0, j].tag;
        for (int i = 0; i < this.width; i++){
          if (tag!="rock"){
            if (tag=="row_power-up" || tag=="column_power-up" || tag=="super_power-up" || tag=="bomb_power-up"){
              return true;
            }
            if (this.elements[i, j]!=null){
              if (this.elements[i, j].tag == tag){
                matchedElements.Add(this.elements[i, j]);
              }
              else{
                if (matchedElements.Count>2){
                  return true;
                }
                tag = this.elements[i, j].tag;
                matchedElements.Clear();
                matchedElements.Add(this.elements[i, j]);
              }
            }
          }
        }
        if (matchedElements.Count>2){
          return true;
        }
        matchedElements.Clear();
      }
    }
    return false;
  }


  public void MatchElements(){
    List<GameObject> matchedElements = new List<GameObject>();
    string tag = "";
    for (int i = 0; i < this.width; i++){
      if (this.elements[i, 0]!=null){
        tag = this.elements[i, 0].tag;
        for (int j = 0; j < this.height; j++){
          if (this.elements[i, j]!=null){
            if (this.elements[i, j].tag == tag){
              matchedElements.Add(this.elements[i, j]);
            }
            else{
              int elemCount = matchedElements.Count;
              List<GameObject> elem = new List<GameObject>();
              if (elemCount>2){
                foreach (GameObject element in matchedElements){
                  if (element!=null){
                    int x = element.GetComponent<Element>().GetColumn();
                    int y = element.GetComponent<Element>().GetRow();
                    for (int saveX = x-1; saveX>=0; saveX--){
                      if (this.elements[saveX, y]!=null){
                        if (this.elements[saveX, y].tag == tag){
                          elem.Add(this.elements[saveX, y]);
                        }
                        else{
                          break;
                        }
                      }
                      else{
                        break;
                      }
                    }
                    for (int saveX = x+1; saveX<this.width; saveX++){
                      if (this.elements[saveX, y]!=null){
                        if (this.elements[saveX, y].tag == tag){
                          elem.Add(this.elements[saveX, y]);
                        }
                        else{
                          break;
                        }
                      }
                      else{
                        break;
                      }
                    }
                  }
                }
                if (elem.Count>1){
                  foreach (GameObject e in elem){
                    matchedElements.Add(e);
                  }
                }
              }
              tag = this.elements[i, j].tag;
              InspectElements(matchedElements);
              matchedElements.Clear();
              matchedElements.Add(this.elements[i, j]);
            }
          }
        }
        InspectElements(matchedElements);
        matchedElements.Clear();
      }
    }
    tag = "";
    for (int j = 0; j < this.height; j++){
      if (this.elements[0, j]!=null){
        tag = this.elements[0, j].tag;
        for (int i = 0; i < this.width; i++){
          if (this.elements[i, j]!=null){
            if (this.elements[i, j].tag == tag){
              matchedElements.Add(this.elements[i, j]);
            }
            else{
              int elemCount = matchedElements.Count;
              List<GameObject> elem = new List<GameObject>();
              if (elemCount>2){
                foreach (GameObject element in matchedElements){
                  if (element!=null){
                    int x = element.GetComponent<Element>().GetColumn();
                    int y = element.GetComponent<Element>().GetRow();
                    for (int saveY = y-1; saveY>=0; saveY--){
                      if (this.elements[x, saveY]!=null){
                        if (this.elements[x, saveY].tag == tag){
                          elem.Add(this.elements[x, saveY]);
                        }
                        else{
                          break;
                        }
                      }
                      else{
                        break;
                      }
                    }
                    for (int saveY = y+1; saveY<this.height; saveY++){
                      if (this.elements[x, saveY]!=null){
                        if (this.elements[x, saveY].tag == tag){
                          elem.Add(this.elements[x, saveY]);
                        }
                        else{
                          break;
                        }
                      }
                      else{
                        break;
                      }
                    }
                  }
                }
                if (elem.Count>1){
                  foreach (GameObject e in elem){
                    matchedElements.Add(e);
                  }
                }
              }
              tag = this.elements[i, j].tag;
              InspectElements(matchedElements);
              matchedElements.Clear();
              matchedElements.Add(this.elements[i, j]);
            }
          }
        }
        InspectElements(matchedElements);
        matchedElements.Clear();
      }
    }
  }

  public bool HasMatches(){
    for (int i = 0; i<this.width; i++){
      for (int j = 0; j<this.height; j++){
        if (this.elements[i, j]!=null){
          if (this.elements[i, j].GetComponent<Element>().IsMatched()){
            return true;
          }
        }
      }
    }
    return false;
  }

  public void Unmatches(){
    for (int i = 0; i<this.width; i++){
      for (int j = 0; j<this.height; j++){
        if (this.elements[i, j]!=null){
          this.elements[i, j].GetComponent<Element>().isMatched = false;
        }
      }
    }
  }

  public void InspectElements(List<GameObject> elemList){
    int x = 0;
    int y = 0;
    bool powerUpCreated = false;
    bool transformed = false;
    int maxColAmount = 0;
    int maxRowAmount = 0;
    elemList.RemoveAll(item => item == null);
    foreach(GameObject e1 in elemList){
      int columnAmount = 1;
      int rowAmount = 1;
      foreach(GameObject e2 in elemList){
        if (e1!=e2){
          if (e1.GetComponent<Element>().GetColumn()==e2.GetComponent<Element>().GetColumn()){
            columnAmount++;
          }
          if (e1.GetComponent<Element>().GetRow()==e2.GetComponent<Element>().GetRow()){
            rowAmount++;
          }
        }
      }
      if (columnAmount>maxColAmount){
        maxColAmount = columnAmount;
      }
      if (rowAmount>maxRowAmount){
        maxRowAmount = rowAmount;
      }
    }
    if (elemList.Count>2){
      if (elemList[0].tag!="column_power-up" && elemList[0].tag!="row_power-up" && elemList[0].tag!="bomb_power-up" && elemList[0].tag!="rock"){
        for(int i = 0; i<elemList.Count; i++){
          x = elemList[i].GetComponent<Element>().GetColumn();
          y = elemList[i].GetComponent<Element>().GetRow();
          if (maxColAmount>2 || maxRowAmount>2){
            if ((elemList[i]==this.swapElement1 || elemList[i]==this.swapElement2) && !powerUpCreated && this.ready){
              DestroyElementAt(x, y);
              if (maxColAmount>=5 || maxRowAmount>=5){
                AddElementAt(x,y, this.superPowerUpPrefab);
                powerUpCreated = true;
                transformed = true;
              }
              else if (maxRowAmount>2 && maxColAmount>2){
                AddElementAt(x,y, this.bombPowerUpPrefab);
                powerUpCreated = true;
                transformed = true;
              }
              else if (maxColAmount>3){
                AddElementAt(x,y, this.rowPowerUpPrefab);
                powerUpCreated = true;
                transformed = true;
              }
              else if (maxRowAmount>3){
                AddElementAt(x,y, this.columnPowerUpPrefab);
                powerUpCreated = true;
                transformed = true;
              }
            }
            else{
              this.elements[x, y].GetComponent<Element>().Match();
            }
          }
          if (!transformed && this.elements[x, y]!=null){
            this.elements[x, y].GetComponent<Element>().Match();
            transformed = false;
          }
        }
        if (elemList.Count>3 && !powerUpCreated){
          x = elemList[0].GetComponent<Element>().GetColumn();
          y = elemList[0].GetComponent<Element>().GetRow();
          DestroyElementAt(x, y);
          if (maxRowAmount>=5 || maxColAmount>=5){
            AddElementAt(x,y, this.superPowerUpPrefab);
          }
          else if (maxRowAmount>2 && maxColAmount>2){
            AddElementAt(x,y, this.bombPowerUpPrefab);
          }
          else if (maxColAmount>3){
            AddElementAt(x,y, this.rowPowerUpPrefab);
          }
          else if (maxRowAmount>3){
            AddElementAt(x,y, this.columnPowerUpPrefab);
          }
        }
      }
    }
  }

  public int GetRockAmount(){
    int amount = 0;
    for (int i = 0; i<this.width; i++){
      for (int j = 0; j<this.height; j++){
        if (this.elements[i, j]!=null){
          if (this.elements[i, j].tag=="rock"){
            amount++;
          }
        }
      }
    }
    return amount;
  }
//[----------------------- MATCH FUNCTIONS -----------------------]


//[----------------------- DESTROY FUNCTIONS -----------------------]
  public void TryBreakRock(GameObject element){
    if (element!=null){
      int x = element.GetComponent<Element>().GetColumn();
      int y = element.GetComponent<Element>().GetRow();
      for (int i = -1; i<2; i+=2){
        if (x+i>=0 && x+i<this.width){
          if (this.elements[x+i, y]!=null){
            if (this.elements[x+i, y].tag=="rock"){
              Destroy(this.elements[x+i, y]);
              this.elements[x+i, y]=null;
              this.score+=10;
            }
          }
        }
      }
      for (int i = -1; i<2; i+=2){
        if (y+i>=0 && y+i<this.height){
          if (this.elements[x, y+i]!=null){
            if (this.elements[x, y+i].tag=="rock"){
              Destroy(this.elements[x, y+i]);
              this.elements[x, y+i]=null;
              this.score+=10;
            }
          }
        }
      }
    }
  }

  public void DestroyMatches(){
    for (int i = 0; i<this.width; i++){
      for (int j = 0; j<this.height; j++){
        if (this.elements[i, j]!=null){
          if (this.elements[i, j].GetComponent<Element>().IsMatched()){
            DestroyElementAt(i, j);
          }
        }
      }
    }
    if (this.powerUpCurrentlyFired==0){
      StartCoroutine(RefillBoard());
    }
  }

  public void DestroyElementAt(int column, int row, bool hitByPowerUp=false){
    bool powerUp = false;
    if (this.elements[column, row]!=null){
      GameObject currentElement = this.elements[column, row];
      if (currentElement!=null){
        if (!currentElement.GetComponent<Element>().isPowerUp && !currentElement.GetComponent<Element>().IsUnbreakable()){
          //[---------ELEMENT ACTIONS---------]
          string tag = currentElement.tag;
          if (tag=="damage" && this.ready){
            this.damageToDeal += this.player.GetDamage();
            this.damage += this.player.GetDamage();
            this.score+=3;
          }
          else if (tag=="magic" && this.ready){
            this.magicToDeal += this.player.GetMagic();
            this.damage += this.player.GetMagic();
            this.score+=3;
          }
          else if (tag=="heal" && this.ready){
            this.player.FillPotionCharge();
            this.score+=1;
          }
          else if (tag=="shield" && this.ready){
            this.player.Block();
            this.score+=1;
          }
          else if (currentElement.tag=="super_power-up" && this.ready){
            powerUp = true;
            if (!currentElement.GetComponent<Element>().IsPowerUpUsed()){
              ActivateSuperPowerUp(this.elements[column, row]);
            }
          }
          if (currentElement.tag=="bomb_power-up" && this.ready){
            powerUp = true;
            if (!currentElement.GetComponent<Element>().IsPowerUpUsed()){
              ActivateBombPowerUp(this.elements[column, row]);
            }
          }
          else if (currentElement.tag=="column_power-up" && this.ready){
            powerUp = true;
            if (!currentElement.GetComponent<Element>().IsPowerUpUsed()){
              ActivatePowerUp(this.elements[column, row]);
            }
          }
          else if (currentElement.tag=="row_power-up" && this.ready){
            powerUp = true;
            if (!currentElement.GetComponent<Element>().IsPowerUpUsed()){
              ActivatePowerUp(this.elements[column, row]);
            }
          }
          //[---------POWER-UP---------]
          if (!powerUp){
            if (this.ready && !hitByPowerUp){
              TryBreakRock(this.elements[column, row]);
            }
          }
          Destroy(this.elements[column, row]);
          this.elements[column, row]=null;
        }
      }
    }
  }

  public IEnumerator EndTutorialLevel(){
    this.transition.SetTrigger("TutorialEnd");
    yield return new WaitForSeconds(2f);
    SceneManager.LoadScene("TutorialScene");
  }

  public IEnumerator ShowWinScreen(){
    while(this.dialoguePrio){
      yield return null;
    }
    this.winScreen.ShowWinScreen(this.level);
    PopUpManager popUpManager = GameObject.Find("Pop Ups").GetComponent<PopUpManager>();
    foreach (Level farmLevel in this.player.GetWorld().GetFarmLevels()){
      if (farmLevel.GetLevelRequirement()==this.player.GetCurrentLevel()){
        popUpManager.AddInfos(farmLevel);
        break;
      }
    }
    int expLevel = this.player.GetExpLevel();
    this.level.RewardPlayer(this.player);
    if (expLevel!=this.player.GetExpLevel()){
      popUpManager.AddInfos(this.player);
    }
    this.player.SetNextLevel();
    this.player.TestFarmLevelsUnlocked();
    popUpManager.Open();
  }

  //[----------------------- DESTROY FUNCTIONS -----------------------]


  public IEnumerator RefillBoard(){
    yield return new WaitForSeconds(0.25f);
    int randomElementIndex = 0;
    int percent = 0;
    bool need = false;
    for (int i = 0; i<this.width; i++){
      for (int j = 0; j<this.height; j++){
        if (this.elements[i, j] == null){
          Vector2 pos = new Vector2(i ,j+offSet);
          percent = Random.Range(0, 100);
          if ((percent<25 && this.GetRockAmount()<5) || (percent<18 && this.GetRockAmount()<10)){
            randomElementIndex = 4;//18%rock (10max)
          }
          else if (percent<33){
            randomElementIndex = 0;//33%magic
          }
          else if (percent<66){
            randomElementIndex = 1;//32%sword
          }
          else if (percent<90){
            randomElementIndex = 2;//14%shield
          }
          else{
            randomElementIndex = 3;//10%heal
          }
          GameObject element = Instantiate(this.elementPrefabs[randomElementIndex], pos, Quaternion.identity);
          element.GetComponent<Element>().SetColumn(i);
          element.GetComponent<Element>().SetRow(j);
          this.elements[i, j] = element;
          need = true;
          yield return null;
        }
        if (need){
          yield return new WaitForSeconds(0.04f);
          need = false;
        }
      }
    }
    yield return new WaitForSeconds(0.2f);
    MatchElements();
    if (HasMatches()){
      DestroyMatches();
    }
    else{
      if (this.level.GetCategory()!="main" && this.level.GetCategory()!="tutorial"){
        if (this.moves==this.level.GetMoves() && !this.rewardsTaken){
          this.rewardsTaken = true;
          this.state = GameState.wait;
          double percentage = 1.0;
          if (this.score<450){
            percentage = 0.2;
          }
          else if (this.score<650){
            percentage = 0.3;
          }
          else if (this.score<900){
            percentage = 0.5;
          }
          else if (this.score<1200){
            percentage = 0.8;
          }
          Dictionary<string, int> rewards = new Dictionary<string, int>();
          foreach(string element in this.level.GetRewards().Keys){
            rewards.Add(element,(int)(this.level.GetRewards()[element]*percentage));
          }
          this.winScreen.ShowWinScreen(this.level, rewards);
          this.player.AddResources(rewards);
        }
        else{
          this.state = GameState.move;
          if (!CheckMatchPossible()){
            Restart(true);
          }
        }
      }
      else{
        if (this.enemy.IsAlive()){
          if (this.animDamage){
            yield return new WaitForSeconds(0.6f);
          }
          if (!this.playerTurn){
            this.playerTurn = true;
            if (this.enemy.Attack(this.player) && this.enemy.GetDamage()>0 && this.enemyCanAttack){
              StartCoroutine(TakeDamageAnimation());
            }
            this.state = GameState.move;
            if (!CheckMatchPossible()){
              Restart(true);
            }
          }
          if (!player.IsAlive()){
            this.state = GameState.wait;
            if (this.level.GetCategory()=="tutorial" && this.player.GetCurrentLevel()==0){
              this.player.SetNextLevel();
              StartCoroutine(EndTutorialLevel());
            }
            else{
              this.loseScreen.SetTrigger("Open");
              this.level.PartialRewardPlayer(this.player);
            }
          }
        }
      }
    }
  }

  public void AddElementAt(int column, int row, GameObject prefab){
    Vector2 pos = new Vector2(column, row + this.offSet);
    if (prefab.tag=="column_power-up" || prefab.tag=="row_power-up" || prefab.tag=="bomb_power-up" || prefab.tag=="super_power-up"){
      pos = new Vector2(column, row);
    }
    GameObject element = Instantiate(prefab, pos, Quaternion.identity);
    element.GetComponent<Element>().SetColumn(column);
    element.GetComponent<Element>().SetRow(row);
    element.transform.parent = this.transform;
    element.name = "(" + column + ", " + row + ")";
    this.elements[column, row] = element;
  }

  public void ClearElements(){
    for (int i = 0; i<this.width; i++){
      for (int j = 0; j<this.height; j++){
        Destroy(this.elements[i, j]);
        this.elements[i ,j] = null;
      }
    }
  }

  //[----------------------- ANIMATION FUNCTIONS -----------------------]
  public IEnumerator TakeDamageAnimation(){
    int damage = (int)(this.enemy.GetDamage() * this.player.GetDamageReduction()) + (int)(this.enemy.GetMagic() * this.player.GetMagicDamageReduction());
    this.damageTaken.GetComponent<TextMeshProUGUI>().enabled = true;
    this.damageTaken.GetComponent<TextMeshProUGUI>().text = "-" + NumberFact.Fact(damage);
    this.damageTaken.GetComponent<Animator>().SetTrigger("TakeDamage");
    yield return new WaitForSeconds(0.7f);
    this.damageTaken.GetComponent<TextMeshProUGUI>().enabled = false;
  }


  // public void InvokeEnemy(int number){
  //   if (this.currentLevel<7){
  //     this.currentLevel = number;
  //     this.enemy = this.world.GetMainLevels()[currentLevel].GetEnemy().GetComponent<Enemy>();
  //     this.enemy.InitBars(this.enemyHealthBar);
  //   }
  //   else{
  //     this.currentLevel = 0;
  //     InvokeEnemy(0);
  //   }
  // }
  //
  // public void ButtonLock(){
  //   if (this.IsLocked()){
  //     Unlock();
  //   }
  //   else{
  //     Lock();
  //   }
  // }
}
