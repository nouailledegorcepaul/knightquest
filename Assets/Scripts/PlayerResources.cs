﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerResources : MonoBehaviour{

    public GameObject playerResources;
    private Player player;
    private GameObject leatherText;
    private GameObject woodText;
    private GameObject stoneText;
    private GameObject magicStoneText;
    private GameObject goldText;

    void Start(){
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
      this.leatherText = this.playerResources.transform.Find("Leather Container").transform.Find("Leather").gameObject;
      this.woodText = this.playerResources.transform.Find("Wood Container").transform.Find("Wood").gameObject;
      this.stoneText = this.playerResources.transform.Find("Stone Container").transform.Find("Stone").gameObject;
      this.magicStoneText = this.playerResources.transform.Find("Magic Stone Container").transform.Find("Magic Stone").gameObject;
      this.goldText = GameObject.Find("Gold Container").transform.Find("Gold").gameObject;
    }

    // Update is called once per frame
    void Update(){
      this.leatherText.GetComponent<TextMeshProUGUI>().text = "" + NumberFact.Fact(this.player.GetLeather());
      this.woodText.GetComponent<TextMeshProUGUI>().text = "" + NumberFact.Fact(this.player.GetWood());
      this.stoneText.GetComponent<TextMeshProUGUI>().text = "" + NumberFact.Fact(this.player.GetStone());
      this.magicStoneText.GetComponent<TextMeshProUGUI>().text = "" + NumberFact.Fact(this.player.GetMagicStone());
      this.goldText.GetComponent<TextMeshProUGUI>().text = "" + NumberFact.Fact(this.player.GetGold());
    }
}
