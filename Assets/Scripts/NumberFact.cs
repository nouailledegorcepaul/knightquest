﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class NumberFact{

    public static string Fact(int number){
      string strNum = number.ToString();
      int numberSize = strNum.Length;
      if (numberSize>=7){
        return ((double)number/1000000).ToString("#.#") + "M";
      }
      if (numberSize>=4){
        return ((double)number/1000).ToString("#.#") + "k";
      }
      return strNum;
    }

    public static string Fact(float fNumber)
    {
      return Fact((int) fNumber);
    }
}
