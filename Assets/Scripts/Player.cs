﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Player : MonoBehaviour{

  [Header("Basic Player Stats")]
  private int maxHealth;
  private int currentHealth;

  public int maxShieldGranted = 5;
  public int maxShieldCharge = 15;
  public int currentShield = 0;

  public int expLevel;
  public int currentExp;
  public int maxExp;
  private int baseHp;
  private int baseDamage;
  private int baseMagic;
  private int baseShield;
  private int baseHeal;
  private int baseDefense;
  private int baseMagicDefense;
  public int upgradePoints;
  public string playerName = "";

  [Header("Player items")]
  public Dictionary<string, Item> equippedItem;
  public Dictionary<string, int> resources;
  public Dictionary<Item, int> inventory;

  [Header("Player Bars")]
  private HealthBar healthBar;
  private ShieldBar shieldBar;
  private PotionCharge potionCharge;
  public ExperienceBar experienceBar;

  public World world;
  public Level level;
  public int currentLevel = 0;
  private static Player instance = null;
  public DialogueBox dialogueBox;
  private Dictionary<int, Dictionary<string, bool>> levelRequirements;
  public bool autoSave;

  private bool farmLevels = false;
  private bool farmTutorial = false;
  private bool saveTutorial = false;
  private bool hasRescuedBlacksmith = false;
  private bool hasRescuedShopKeeper = false;
  private bool hasRescuedArtisan = false;
  private bool hasRescuedDoctor = false;
  private string saveFileName = "";

  //[------------------- INITALISATION FUNCTIONS --------------------]
  void Awake(){
    if (instance==null){
      Player.instance = this;
      SaveSystem.InitSaveDictionary();
      DontDestroyOnLoad(this.gameObject);
    }
    else{
      Destroy(this.gameObject);
    }
  }

  void Start(){
    Init();
  }

  public void Init(){
    DataBase.SetAllId();
    this.baseHp = 1500000;
    this.baseDamage = 10000;
    this.baseMagic = 10000;
    this.baseShield = 2000;
    this.baseHeal = 2000;
    this.baseDefense = 120;
    this.baseMagicDefense = 80;
    this.expLevel = 1;
    this.currentExp = 0;
    this.maxExp = 50;
    this.currentLevel = 0;
    this.level = this.world.GetLevels()[this.currentLevel];
    this.upgradePoints = 0;
    this.InitPlayer();

  }

  void Update(){
    if (this.healthBar != null){
      this.healthBar.SetHealth(this.currentHealth);
    }
  }

  public void InitPlayer(){
    this.equippedItem = new Dictionary<string, Item>();
    this.resources = new Dictionary<string, int>();
    this.inventory = new Dictionary<Item, int>();

    this.resources.Add("wood", 0);
    this.resources.Add("leather", 0);
    this.resources.Add("stone", 0);
    this.resources.Add("magic_stone", 0);
    this.resources.Add("gold", 0);

    this.equippedItem["head"] = null;
    this.equippedItem["body"] = null;
    this.equippedItem["pants"] = null;
    this.equippedItem["boots"] = null;

    this.equippedItem["damage"] = null;
    this.equippedItem["magic"] = null;
    this.equippedItem["shield"] = null;
    this.equippedItem["heal"] = null;

    Item headStarter = Resources.Load<Item>("ScriptableObjects/Items/Head/BasicLeatherHelmet");
    Item bodyStarter = Resources.Load<Item>("ScriptableObjects/Items/Body/BasicLeatherJacket");
    Item pantsStarter = Resources.Load<Item>("ScriptableObjects/Items/Pants/BasicLeatherPants");
    Item bootsStarter = Resources.Load<Item>("ScriptableObjects/Items/Boots/BasicLeatherBoots");

    this.AddInventory(headStarter);
    this.SetItem(headStarter);
    this.AddInventory(bodyStarter);
    this.SetItem(bodyStarter);
    this.AddInventory(pantsStarter);
    this.SetItem(pantsStarter);
    this.AddInventory(bootsStarter);
    this.SetItem(bootsStarter);

    Item damageStarter = Resources.Load<Item>("ScriptableObjects/Items/Damage/BasicWoodenStick");
    Item magicStarter = Resources.Load<Item>("ScriptableObjects/Items/Magic/BasicWoodenWand");
    Item shieldStarter = Resources.Load<Item>("ScriptableObjects/Items/Shield/BasicWoodenPlank");
    Item healStarter = Resources.Load<Item>("ScriptableObjects/Items/Heal/DirtyMedicinalHerbs");

    this.AddInventory(damageStarter);
    this.SetItem(damageStarter);
    this.AddInventory(magicStarter);
    this.SetItem(magicStarter);
    this.AddInventory(shieldStarter);
    this.SetItem(shieldStarter);
    this.AddInventory(healStarter);
    this.SetItem(healStarter);

    this.levelRequirements = new Dictionary<int, Dictionary<string, bool>>();
  }

  public void InitBars(GameObject healthBar, GameObject shieldBar, GameObject potionCharge){
    this.maxHealth = this.GetHp();
    this.healthBar = healthBar.GetComponent<HealthBar>();
    this.currentHealth = this.maxHealth;

    this.healthBar.SetMaxHealth(this.maxHealth);
    this.healthBar.SetHealth(this.currentHealth);

    this.InitShieldVariables();
    this.shieldBar = shieldBar.GetComponent<ShieldBar>();
    this.shieldBar.SetMaxShield(this.maxShieldCharge);
    this.shieldBar.SetShield(0);

    this.potionCharge = potionCharge.GetComponent<PotionCharge>();
    this.potionCharge.SetPlayer(this);
    this.InitHealVariables();
  }

  public void InitShieldVariables(){
    this.maxShieldCharge = this.baseShield*25 + 12*this.equippedItem["shield"].GetShield();
    this.maxShieldGranted = (int)(this.maxShieldCharge * 1.05);
    this.currentShield = 0;
  }

  public void InitHealVariables(){
    this.potionCharge.SetMaxFillAmount( this.baseHeal*12 + 18*this.equippedItem["heal"].GetHeal());
    this.potionCharge.SetChargeHealAmount( (int)( GetMaxHealth()*(0.15*(2-GetHealMult())) ) );

  }
  //[------------------- INITALISATION FUNCTIONS --------------------]

  //[----------------------- GETTER FUNCTIONS -----------------------]
  public string GetName(){return this.playerName;}

  public int GetCurrentHealth(){return this.currentHealth;}
  public int GetMaxHealth(){return this.maxHealth;}
  public int GetMaxShieldGranted(){return this.maxShieldGranted;}
  public int GetMaxShieldCharge(){return this.maxShieldCharge;}

  public int GetBaseHp(){return this.baseHp;}
  public int GetBaseDamage(){return this.baseDamage;}
  public int GetBaseMagic(){return this.baseMagic;}
  public int GetBaseShield(){return this.baseShield;}
  public int GetBaseHeal(){return this.baseHeal;}
  public int GetBaseDefense(){return this.baseDefense;}
  public int GetBaseMagicDefense(){return this.baseMagicDefense;}

  public int GetHp(){
    return this.baseHp + this.equippedItem["head"].GetHp() + this.equippedItem["body"].GetHp() + this.equippedItem["pants"].GetHp() +
            this.equippedItem["boots"].GetHp();
  }
  public int GetDamage(){return this.baseDamage + this.equippedItem["damage"].GetDamage() + this.equippedItem["damage"].GetMagic();}
  public int GetMagic(){return this.baseMagic + this.equippedItem["magic"].GetMagic()+this.equippedItem["magic"].GetDamage();}
  public int GetShield(){return this.baseShield + this.equippedItem["shield"].GetShield();}
  public int GetHeal(){return this.baseHeal + this.equippedItem["heal"].GetHeal();}
  public int GetDefense(){
    return this.baseDefense + this.equippedItem["head"].GetDefense() + this.equippedItem["body"].GetDefense() + this.equippedItem["pants"].GetDefense() +
            this.equippedItem["boots"].GetDefense();
  }
  public int GetMagicDefense(){
    return this.baseMagicDefense + this.equippedItem["head"].GetMagicDefense() + this.equippedItem["body"].GetMagicDefense() + this.equippedItem["pants"].GetMagicDefense() +
            this.equippedItem["boots"].GetMagicDefense();
  }

  public int GetGold(){return this.resources["gold"];}
  public int GetActiveShield(){return this.healthBar.GetShield();}
  public Dictionary<string, Item> GetEquippedItem(){return this.equippedItem;}

  public Item GetHeadItem(){return this.equippedItem["head"];}
  public Item GetBodyItem(){return this.equippedItem["body"];}
  public Item GetPantsItem(){return this.equippedItem["pants"];}
  public Item GetBootsItem(){return this.equippedItem["boots"];}
  public Item GetDamageItem(){return this.equippedItem["damage"];}

  public Item GetMagicItem(){return this.equippedItem["magic"];}
  public Item GetShieldItem(){return this.equippedItem["shield"];}
  public Item GetHealItem(){return this.equippedItem["heal"];}

  public Dictionary<string, int> GetResources(){return this.resources;}
  public Dictionary<Item, int> GetInventory(){return this.inventory;}

  public int GetLeather(){return this.resources["leather"];}
  public int GetWood(){return this.resources["wood"];}
  public int GetStone(){return this.resources["stone"];}
  public int GetMagicStone(){return this.resources["magic_stone"];}

  public int GetUpgradePoints(){return this.upgradePoints;}
  public bool IsAlive(){return this.currentHealth>0;}
  public int GetCurrentLevel(){return this.currentLevel;}
  public int GetExpLevel(){return this.expLevel;}
  public int GetCurrentExp(){return this.currentExp;}
  public int GetMaxExp(){return this.maxExp;}

  public World GetWorld(){return this.world;}
  public Level GetLevel(){return this.level;}

  public Dictionary<int, Dictionary<string, bool>> GetLevelRequirements(){return this.levelRequirements;}


  public DialogueBox GetDialogueBox(){
    if (this.dialogueBox==null){
      this.dialogueBox = GameObject.Find("Dialogue Box").GetComponent<DialogueBox>();
    }
    return this.dialogueBox;
  }

  public bool RecipeComplete(Dictionary<string, int> recipe){
    if (recipe==null){
      return false;
    }
    foreach (string element in recipe.Keys){
      if (recipe[element]>this.resources[element]){
        return false;
      }
    }
    return true;
  }

  public bool HasCrafted(Item item){
    if (item!=null){
      return this.inventory.ContainsKey(item);
    }
    return false;
  }

  public bool HasItem(Item item){
    if (HasCrafted(item)){
      return this.inventory[item]>0;
    }
    return false;
  }

  public bool HasResources(string element, int amount = 0){
    if (this.resources.ContainsKey(element)){
      return this.resources[element]>=amount;
    }
    return false;
  }

  public bool FarmTutorialDone(){return this.farmTutorial;}
  public bool SaveTutorialDone(){return this.saveTutorial;}
  public bool HasRescuedBlacksmith(){return this.hasRescuedBlacksmith;}
  public bool HasRescuedShopKeeper(){return this.hasRescuedShopKeeper;}
  public bool HasRescuedArtisan(){return this.hasRescuedArtisan;}
  public bool HasRescuedDoctor(){return this.hasRescuedDoctor;}
  public bool HasFarmLevelsUnlocked(){return this.farmLevels;}
  public bool AutoSaveEnabled(){return this.autoSave;}

  public string GetSaveFileName(){return this.saveFileName;}
  public bool TestFarmCategorieUnlocked(string category){
    if (this.world.GetFarmLevel(category).GetLevelRequirement()<this.currentLevel){
      return true;
    }
    return false;
  }

  public void TestFarmLevelsUnlocked(){
    if (!this.farmLevels){
      foreach (Level farmLevel in this.world.GetFarmLevels()){
        if (farmLevel.GetLevelRequirement()<this.currentLevel){
          this.farmLevels = true;
        }
      }
    }
  }

  public double GetHealMult(){return 100.0/(100.0+GetHeal());}
  public double GetDamageReduction(){return 100.0/(100.0+GetDefense());}
  public double GetMagicDamageReduction(){return 100.0/(100.0+GetMagicDefense());}
  //[----------------------- GETTER FUNCTIONS -----------------------]


  //[----------------------- SETTER FUNCTIONS -----------------------]
  public void SetPlayerName(string name){this.playerName = name;}

  public void AddInventory(Item item){
    if (!this.inventory.ContainsKey(item)){
      this.inventory.Add(item, 1);
    }
    else{
      this.inventory[item]++;
    }
  }
  public void AddHealth(int health){
    this.currentHealth += health;
    if (this.currentHealth > this.maxHealth){
      this.currentHealth = this.maxHealth;
    }
  }

  public void AddExperience(int exp){
    if (this.currentExp+exp>=this.maxExp){
      exp -= (int)(this.maxExp - this.currentExp);
      this.LevelUp();
    }
    this.currentExp+=exp;
  }

  public void LevelUp(){
    this.expLevel++;
    this.currentExp=0;
    double multiplicateur;
    if (this.expLevel<10){
      multiplicateur = 1.5;
    }
    else if (this.expLevel<20){
      multiplicateur = 1.4;
    }
    else{
      multiplicateur = 1.3;
    }
    this.maxExp = (int)(maxExp*multiplicateur);
    this.AddUpgradePoint();
  }

  public void Craft(Dictionary<string, int> recipe){
    foreach(string element in recipe.Keys){
      this.SubResources(element, recipe[element]);
    }
  }

  public void SetItem(Item item){
    string type = item.GetItemType();
    if (this.equippedItem[type]!=null){
      this.AddInventory(this.equippedItem[type]);
    }
    this.equippedItem[type] = item;
    this.inventory[item]--;
  }

  public void SetGold(int gold){this.resources["gold"] = gold;}
  public void AddGold(int gold){this.resources["gold"] += gold;}
  public void SubGold(int gold){this.resources["gold"] -= gold;}
  public void SubPlayerShield(int amount){this.healthBar.SubShield(amount);}
  public void AddPlayerShield(int amount){this.healthBar.AddShield(amount);}
  public void AddUpgradePoint(){this.upgradePoints++;}

  public void AddBaseStats(string stat){
    int amount = 0;
    if (this.expLevel<10){
      amount = 1;
    }
    else if (this.expLevel<20){
      amount = 3;
    }
    else if (this.expLevel<30){
      amount = 6;
    }
    switch(stat){
      case "damage":
        this.baseDamage+=amount;
        this.upgradePoints--;
        break;
      case "magic":
        this.baseMagic+=amount;
        this.upgradePoints--;
        break;
      case "shield":
        this.baseShield+=amount;
        this.upgradePoints--;
        break;
      case "heal":
        this.baseHeal+=amount;
        this.upgradePoints--;
        break;
    }
  }

  public void SubBaseStats(string stat){
    int amount = 0;
    if (this.expLevel<10){
      amount = 1;
    }
    else if (this.expLevel<20){
      amount = 3;
    }
    else if (this.expLevel<30){
      amount = 6;
    }
    switch(stat){
      case "damage":
        this.baseDamage-=amount;
        this.upgradePoints++;
        break;
      case "magic":
        this.baseMagic-=amount;
        this.upgradePoints++;
        break;
      case "shield":
        this.baseShield-=amount;
        this.upgradePoints++;
        break;
      case "heal":
        this.baseHeal-=amount;
        this.upgradePoints++;
        break;
    }
  }

  public void SetNextLevel(){
    if (this.currentLevel<this.world.GetLevels().Count-1){
      this.currentLevel += 1;
      if (currentLevel==1){
        this.baseHp = 400;
        this.baseDamage = 10;
        this.baseMagic = 10;
        this.baseShield = 5;
        this.baseHeal = 5;
        this.baseDefense = 10;
        this.baseMagicDefense = 10;
      }
      this.level = this.world.GetLevels()[this.currentLevel];
      if (this.level.GetRequierement()!=null){
        this.levelRequirements.Add(this.currentLevel, new Dictionary<string, bool>());
        this.levelRequirements[this.currentLevel].Add("first", true);
        this.levelRequirements[this.currentLevel].Add("end", false);
        this.levelRequirements[this.currentLevel].Add("complete", false);
      }
    }
    this.currentShield = 0;
  }

  public void AddResources(Dictionary<string, int> resources){
    foreach (string type in resources.Keys){
      this.AddResources(type, resources[type]);
    }
  }

  public void AddResources(string type, int amount = 1){
    if (type=="exp"){
      AddExperience(amount);
    }
    else{
      if (this.resources.ContainsKey(type)){
        this.resources[type]+=amount;
      }
      else{
        this.resources.Add(type, amount);
      }
    }
  }

  public void SubResources(string type, int amount = 1){this.resources[type]-=amount;}
  public void SetLevel(Level level){this.level = level;}
  public void FarmTutorial(){this.farmTutorial = true;}
  public void SaveTutorial(){this.saveTutorial = true;}
  public void ShopKeeperRescued(){this.hasRescuedShopKeeper = true;}
  public void BlacksmithRescued(){this.hasRescuedBlacksmith = true;}
  public void ArtisanRescued(){this.hasRescuedArtisan = true;}
  public void DoctorRescued(){this.hasRescuedDoctor = true;}
  public void SetSaveFileName(string saveFileName){this.saveFileName = saveFileName;}
  public void SetLevelRequirement(string type, bool value){this.levelRequirements[this.currentLevel][type]=value;}
  public void SetAutoSave(bool autoSave){this.autoSave = autoSave;}
  //[----------------------- SETTER FUNCTIONS -----------------------]


  //[----------------------- ACTION FUNCTIONS -----------------------]
  public void Attack(int amount, string type, Enemy enemy){
    if (type=="damage"){
      enemy.TakeDamage(type, amount);
    }
    else{
      enemy.TakeDamage(type, amount);
    }
  }

  public void FillPotionCharge(){this.potionCharge.AddCurrentCharge(this.GetHeal());}
  public void Heal(){this.potionCharge.UseCharge();}

  public void Block(){
    if (this.GetActiveShield()==0){
      this.currentShield += this.GetShield();
      if (this.currentShield > this.maxShieldCharge){
        this.currentShield = this.maxShieldCharge;
      }
      this.shieldBar.SetShield(this.currentShield);
      if (this.currentShield==this.maxShieldCharge){
        this.healthBar.SetMaxShield(this.maxShieldGranted);
        this.healthBar.SetShield(this.maxShieldGranted);
        this.currentShield = 0;
        this.shieldBar.SetShield(0);
      }
    }
  }

  public void TakeDamage(Enemy enemy){
    int enemyDamage = (int)(enemy.GetDamage() * GetDamageReduction());
    int enemyMagic = (int)(enemy.GetMagic() * GetMagicDamageReduction());
    if (this.GetActiveShield() > enemy.GetDamage()){
      this.healthBar.SubShield(enemy.GetDamage());
      enemyDamage = 0;
    }
    else{
      enemyDamage -= this.GetActiveShield();
      this.healthBar.SetShield(0);
    }
    if (this.GetActiveShield() > enemy.GetMagic()){
      this.healthBar.SubShield(enemy.GetMagic());
      enemyMagic = 0;
    }
    else{
      enemyMagic -= this.GetActiveShield();
      this.healthBar.SetShield(0);
    }
    this.currentHealth -= enemyDamage;
    this.currentHealth -= enemyMagic;
  }

  public void Reset(){
    this.currentHealth = this.maxHealth;
    this.currentShield = 0;
    this.shieldBar.Reset();
    this.potionCharge.Reset();
  }
  //[----------------------- ACTION FUNCTIONS -----------------------]
  public void SavePlayer(string path="new_save"){
    SaveSystem.SavePlayer(this, path);
    this.SetSaveFileName(path);
  }

  public void LoadPlayer(string path){
    PlayerData data = SaveSystem.LoadPlayer(path);
    this.maxHealth = data.maxHealth;
    this.currentHealth = data.maxHealth;
    this.maxShieldGranted = data.maxShieldGranted;
    this.maxShieldCharge = data.maxShieldCharge;
    this.expLevel = data.expLevel;
    this.maxExp = data.maxExp;
    this.currentExp = data.currentExp;
    this.baseDamage = data.baseDamage;
    this.baseMagic = data.baseMagic;
    this.baseShield = data.baseShield;
    this.baseHeal = data.baseHeal;
    this.baseHp = data.baseHp;
    this.baseDefense = data.baseDefense;
    this.baseMagicDefense = data.baseMagicDefense;
    this.upgradePoints = data.upgradePoints;
    this.playerName = data.playerName;

    this.equippedItem = new Dictionary<string, Item>();
    this.equippedItem["head"] = null;
    this.equippedItem["body"] = null;
    this.equippedItem["pants"] = null;
    this.equippedItem["boots"] = null;
    this.equippedItem["damage"] = null;
    this.equippedItem["magic"] = null;
    this.equippedItem["shield"] = null;
    this.equippedItem["heal"] = null;
    foreach(string element in data.equippedItem.Keys){
      int id = data.equippedItem[element];
      Item item = DataBase.GetItemById(id);
      this.AddInventory(item);
      this.SetItem(item);
    }
    this.resources = data.resources;
    this.inventory = new Dictionary<Item, int>();
    foreach(int id in data.inventory.Keys){
      Item item = DataBase.GetItemById(id);
      this.inventory.Add(item, 0);
      for (int i = 0; i<(data.inventory[id]); i++){
        this.AddInventory(item);
      }
    }
    this.resources = data.resources;
    this.inventory = new Dictionary<Item, int>();
    foreach(int id in data.inventory.Keys){
      Item item = DataBase.GetItemById(id);
      this.inventory.Add(item, 0);
      for (int i = 0; i<(data.inventory[id]); i++){
        this.AddInventory(item);
      }
    }
    this.levelRequirements = data.levelRequirements;
    this.currentLevel = data.currentLevel;
    this.level = this.world.GetLevels()[this.currentLevel];
    this.hasRescuedBlacksmith = data.hasRescuedBlacksmith;
    this.hasRescuedShopKeeper = data.hasRescuedShopKeeper;
    this.hasRescuedArtisan = data.hasRescuedArtisan;
    this.hasRescuedDoctor = data.hasRescuedDoctor;
    this.farmLevels = data.farmLevels;
    this.farmTutorial = data.farmTutorial;
    this.saveTutorial = data.saveTutorial;
    this.autoSave = data.autoSave;
  }
}
