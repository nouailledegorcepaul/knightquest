﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueBox : MonoBehaviour{

  public Dialogue dialogue;
  private TextMeshProUGUI dialogueText;
  private TextMeshProUGUI nameText;
  private Button dialogueButton;
  private Animator animator;
  private Queue<string> sentences;
  private bool dialogueEnded;
  private Player player;

  void Start(){
    this.player = GameObject.FindWithTag("player").GetComponent<Player>();
    this.sentences = new Queue<string>();
    this.animator = this.gameObject.GetComponent<Animator>();
    this.nameText = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    this.dialogueText = this.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
    this.dialogueButton = this.transform.GetChild(2).GetComponent<Button>();
    this.dialogueButton.onClick.AddListener(() => NextSentence(true));
  }

  public void TutorialModSwap(){
    this.dialogueButton.onClick.RemoveAllListeners();
    this.dialogueButton.onClick.AddListener(() => NextSentence(false));
  }

  public void StartDialogue(Dialogue dialogue, bool tutorial=false){
    this.dialogueEnded = false;
    this.animator.SetBool("isOpened", true);
    this.nameText.text = dialogue.GetSpeakerName();
    this.sentences.Clear();

    foreach (string sentence in dialogue.GetSentences()){
      this.sentences.Enqueue(sentence);
    }
    NextSentence(tutorial);
  }

  public void NextSentence(bool tutorial){
    if (this.sentences.Count==0){
      this.animator.SetBool("isOpened", false);
      this.dialogueEnded = true;
    }
    else{
      string sentence = this.sentences.Dequeue();
      StopAllCoroutines();
      StartCoroutine(DisplayText(sentence, tutorial));
    }
  }

  public IEnumerator DisplayText(string sentence, bool tutorial){
    sentence = sentence.Replace("$name", this.player.GetName());
    this.dialogueText.text = "";
    foreach (char letter in sentence.ToCharArray()){
      this.dialogueText.text += letter;
      if (tutorial){
        yield return new WaitForSeconds(0.03f);
      }
      else{
        yield return null;
      }
    }
  }

  public Dialogue GetDialogue(){
    return this.dialogue;
  }

  public void SetDialogue(Dialogue dialogue){
    this.dialogue = dialogue;
  }

  public bool DialogueEnded(){
    return this.dialogueEnded;
  }
}
