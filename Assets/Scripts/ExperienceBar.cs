using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ExperienceBar : MonoBehaviour{

  public Slider slider;
  public Image fill;
  public Gradient gradient;
  public GameObject textExp;
  public GameObject textLevel;

  public Player player;

  void Start(){
    this.player = GameObject.FindWithTag("player").GetComponent<Player>();
    this.fill.color = gradient.Evaluate(1f);
  }


  void Update(){
    this.slider.maxValue = this.player.GetMaxExp();
    this.slider.value = this.player.GetCurrentExp();
    this.fill.color = gradient.Evaluate(slider.normalizedValue);
    this.textExp.GetComponent<TextMeshProUGUI>().SetText(this.slider.value + "/" + this.slider.maxValue);
    this.textLevel.GetComponent<TextMeshProUGUI>().text = "Lvl "+this.player.GetExpLevel();
  }

}
