using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public static class SaveSystem{

  public static Dictionary<string, string> saves = new Dictionary<string, string>();

  public static void SavePlayer(Player player, string customPath){
    BinaryFormatter formatter = new BinaryFormatter();
    string cleanCustomPath = "";
    for (int i = 0; i<customPath.Length; i++){
      if (Char.IsLetter(customPath[i]) || (customPath[i] + "" =="-")){
        cleanCustomPath += customPath[i];
      }
    }
    customPath = cleanCustomPath;
    string path = Application.persistentDataPath + "/" + customPath + ".save";
    if (SaveSystem.saves.ContainsKey(customPath)){
      DeletePlayer(SaveSystem.saves[customPath]);
      SaveSystem.saves[customPath] = path;
    }
    else{
      SaveSystem.saves.Add(customPath, path);
    }

    FileStream stream = new FileStream(path, FileMode.Create);

    PlayerData data = new PlayerData(player);

    formatter.Serialize(stream, data);
    stream.Close();
  }

  public static PlayerData LoadPlayer(string path){
    if (File.Exists(path)){
      BinaryFormatter formatter = new BinaryFormatter();
      FileStream stream = new FileStream(path, FileMode.Open);

      PlayerData data = formatter.Deserialize(stream) as PlayerData;
      stream.Close();

      return data;
    }
    else{
      Debug.LogError("Save file not found in "+path);
      return null;
    }
  }

  public static void DeletePlayer(string path){
    File.Delete(path);
  }

  public static void InitSaveDictionary(){
    foreach (string path in Directory.GetFiles(Application.persistentDataPath, "*.save")){
      string name = "";
      for (int i = path.Length-6; i>0; i--){
        if (path[i]+""=="\\" || path[i]+""=="/"){
          break;
        }
        if(Char.IsLetter(path[i])){
          name = path[i] + name;
        }
      }
      SaveSystem.saves.Add(name, path);
    }
  }

}
