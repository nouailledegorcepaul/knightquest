﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using TMPro;

public class FileLoader : MonoBehaviour{

    private Player player;
    public GameObject saveFilesContainer;
    public GameObject saveFilesTemplate;
    private string[] saveFiles;

    void Start(){
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
      this.saveFiles = Directory.GetFiles(Application.persistentDataPath, "*.save");
      this.DisplaySaveFiles();
    }

    void Update(){
      if (Input.GetKeyDown(KeyCode.Escape)){
        if (this.player.GetName()==""){
          SceneManager.LoadScene("MainMenuScene");
        }
        else{
          SceneManager.LoadScene("KingdomScene");
        }
      }
    }

    void DisplaySaveFiles(){
      for (int i = this.saveFiles.Length-1; i>=0; i--){
        string path = this.saveFiles[i];
        GameObject container = Instantiate(this.saveFilesTemplate, this.transform.position, Quaternion.identity);
        container.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = this.GetFileName(path);
        string date = File.GetLastWriteTime(@""+path).ToString();
        container.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = date;
        container.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => LoadGame(path));
        container.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(() => DeleteGame(container, path));
        container.transform.SetParent(this.saveFilesContainer.transform);
        container.transform.localScale = new Vector3(1f, 1f, 1f);
      }
    }

    public string GetFileName(string file){
      string res = "";
      for (int i = file.Length-6; i!=-1; i--){
        if (file[i]=='\\' || file[i]=='/'){
          return res;
        }
        res = res.Insert(0, file[i]+"");
      }
      return res;
    }

    public void LoadGame(string path){
      this.player.LoadPlayer(path);
      this.player.SetSaveFileName(this.GetFileName(path));
      SceneManager.LoadScene("KingdomScene");
    }

    public void DeleteGame(GameObject container, string path){
      SaveSystem.DeletePlayer(path);
      Destroy(container);
    }
}
