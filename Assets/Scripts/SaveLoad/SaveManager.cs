﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour{

    private Player player;
    public Animator animator;
    public QuitWindow quitWindow;
    public SaveInputWindow saveInputWindow;
    private bool isOpened;
    private bool antiSpam = false;
    private bool quitWindowOpened = false;
    private bool saveInputWindowOpened = false;

    void Start(){
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
    }

    void Update(){
      if (Input.GetKeyDown(KeyCode.Escape) && !antiSpam){
        if (!quitWindowOpened && !saveInputWindowOpened){
          this.antiSpam = true;
          StartCoroutine(AntiSpamCD());
          if (isOpened){
            this.isOpened = false;
            this.animator.SetTrigger("Close");
          }
          else{
            this.isOpened = true;
            this.animator.SetTrigger("Open");
          }
        }
        else if (quitWindowOpened){
          this.CloseQuitWindow();
        }
        else if (saveInputWindowOpened){
          this.CloseSaveInputWindow();
        }
      }
    }

    public IEnumerator AntiSpamCD(){
      yield return new WaitForSeconds(0.7f);
      this.antiSpam = false;
    }

    public void OpenQuitWindow(){
      this.quitWindowOpened = true;
      this.quitWindow.Open();
    }

    public void CloseQuitWindow(){
      this.quitWindowOpened = false;
      this.quitWindow.Close();
    }

    public void OpenSaveInputWindow(){
      this.saveInputWindowOpened = true;
      this.saveInputWindow.Open();
    }

    public void CloseSaveInputWindow(){
      this.saveInputWindowOpened = false;
      this.saveInputWindow.Close();
    }

    public void GoToLoadScene(){
      SceneManager.LoadScene("LoadScene");
    }

}
