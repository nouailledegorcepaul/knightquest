using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SaveInputWindow : MonoBehaviour{

    public TextMeshProUGUI inputText;
    public TMP_InputField inputField;
    public Animator animator;
    private Player player;

    void Start(){
      this.player = GameObject.FindWithTag("player").GetComponent<Player>();
      this.inputField.characterLimit = 15;
      this.inputField.text = this.player.GetSaveFileName();
    }

    void Update(){

    }

    public void Save(){
      if (inputText.text!=""){
        string fileName = this.inputText.text;
        this.player.SavePlayer(fileName);
        GameObject.Find("Save Screen").GetComponent<SaveManager>().CloseSaveInputWindow();
      }
    }

    public void Close(){
      this.animator.SetTrigger("Close");
    }

    public void Open(){
      this.animator.SetTrigger("Open");

    }
}
