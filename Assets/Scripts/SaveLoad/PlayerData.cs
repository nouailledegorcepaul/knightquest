﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class PlayerData{
  public int maxHealth;

  public int maxShieldGranted;
  public int maxShieldCharge;

  public int expLevel;
  public int currentExp;
  public int maxExp;
  public int baseDamage;
  public int baseMagic;
  public int baseShield;
  public int baseHeal;
  public int baseHp;
  public int baseDefense;
  public int baseMagicDefense;
  public int upgradePoints;
  public string playerName;

  public Dictionary<string, int> equippedItem;
  public Dictionary<string, int> resources;
  public Dictionary<int, int> inventory;
  public Dictionary<int, Dictionary<string, bool>> levelRequirements;

  public int currentLevel;
  public bool hasRescuedBlacksmith;
  public bool hasRescuedShopKeeper;
  public bool hasRescuedArtisan;
  public bool hasRescuedDoctor;
  public bool farmLevels;
  public bool farmTutorial;
  public bool saveTutorial;
  public bool autoSave;

  public PlayerData(Player player){
    this.maxHealth = player.GetMaxHealth();
    this.maxShieldGranted = player.GetMaxShieldGranted();
    this.maxShieldCharge = player.GetMaxShieldCharge();
    this.expLevel = player.GetExpLevel();
    this.currentExp = player.GetCurrentExp();
    this.maxExp = player.GetMaxExp();
    this.baseDamage = player.GetBaseDamage();
    this.baseMagic = player.GetBaseMagic();
    this.baseShield = player.GetBaseShield();
    this.baseHeal = player.GetBaseHeal();
    this.baseHp = player.GetBaseHp();
    this.baseDefense = player.GetBaseDefense();
    this.baseMagicDefense = player.GetBaseMagicDefense();
    this.upgradePoints = player.GetUpgradePoints();
    this.playerName = player.GetName();
    this.equippedItem = new Dictionary<string, int>();
    foreach(string element in player.GetEquippedItem().Keys){
      int id = player.GetEquippedItem()[element].GetId();
      this.equippedItem.Add(element, id);
    }
    this.resources = player.GetResources();
    this.inventory = new Dictionary<int, int>();
    foreach(Item item in player.GetInventory().Keys){
      int id = item.GetId();
      int amount = player.GetInventory()[item];
      this.inventory.Add(id, amount);
    }
    this.levelRequirements = new Dictionary<int, Dictionary<string, bool>>();
    foreach(int level in player.GetLevelRequirements().Keys){
      this.levelRequirements.Add(level, new Dictionary<string, bool>());
      foreach(string type in player.GetLevelRequirements()[level].Keys){
        this.levelRequirements[level].Add(type, player.GetLevelRequirements()[level][type]);
      }
    }
    this.currentLevel = player.GetCurrentLevel();
    this.hasRescuedBlacksmith = player.HasRescuedBlacksmith();
    this.hasRescuedShopKeeper = player.HasRescuedShopKeeper();
    this.hasRescuedArtisan = player.HasRescuedArtisan();
    this.hasRescuedDoctor = player.HasRescuedDoctor();
    this.farmLevels = player.HasFarmLevelsUnlocked();
    this.farmTutorial = player.FarmTutorialDone();
    this.saveTutorial = player.SaveTutorialDone();
    this.autoSave = player.AutoSaveEnabled();
  }
}
