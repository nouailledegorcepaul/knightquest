﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour{

  public Animator transition;

  public void Play(){
    SceneManager.LoadScene("Arcade");
    // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // Load Scene index+1
  }
  public void InitGame(){
    StartCoroutine(GoToInputName());
  }

  public IEnumerator GoToInputName(){
    transition.SetTrigger("Play");
    yield return new WaitForSeconds(2f);
    SceneManager.LoadScene("InputNameScene");
  }

  public void PlayWithFade(){
    StartCoroutine(StartGameWithFade());
  }

  public IEnumerator StartGameWithFade(){
    transition.SetTrigger("Play");
    yield return new WaitForSeconds(1f);
    SceneManager.LoadScene("ArcadeScene");
  }

  public void PlayKingdom(){
    StartCoroutine(GoToKingdom());
  }

  public IEnumerator GoToKingdom(){
    transition.SetTrigger("Play");
    yield return new WaitForSeconds(1f);
    SceneManager.LoadScene("KingdomScene");
  }

  public void LoadMenu(){
    SceneManager.LoadScene("LoadScene");
  }

  public void Quit(){
    Debug.Log("Quit");
    Application.Quit();
  }

  public void GameMainMenu(){
    SceneManager.LoadScene("MainMenuScene");
  }
}
